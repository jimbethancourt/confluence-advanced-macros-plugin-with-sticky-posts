package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.RenderContext;

import java.util.Map;

public class DocMacro extends BaseMacro
{
    public boolean isInline()
    {
        return true;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.INLINE.or(RenderMode.allow(RenderMode.F_MACROS));
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        // Set the baseurl for now we'll hardcode it. Should be configurable later on.
        String baseurl = "http://confluence.atlassian.com/";
        // get the link parameter
        String relativeLink = (String) parameters.get("0");

        return "<a href=\"" + baseurl + GeneralUtil.htmlEncode(relativeLink) + "\" target=\"_blank\">" + body + "</a>";
    }

    public boolean suppressSurroundingTagDuringWysiwygRendering()
    {
        return true;
    }

    public boolean suppressMacroRenderingDuringWysiwyg()
    {
        return false;
    }
}
