package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.query.SearchQueryInterpreter;
import com.atlassian.confluence.macro.query.SearchQueryInterpreterException;
import com.atlassian.confluence.macro.query.params.BooleanQueryFactoryParameter;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.LabelQuery;
import com.atlassian.spring.container.ContainerManager;


/**
 * Created with IntelliJ IDEA.
 * User: jimb
 * Date: 4/17/12
 * Time: 10:44 AM
 */
public class StickyLabelParameter extends BooleanQueryFactoryParameter
{

    public static final String[] PARAM_NAME = {"sticky"};

    private LabelManager labelManager;

    public StickyLabelParameter()
    {
        this(null);
    }

    public StickyLabelParameter(String defaultValue)
    {
        super(PARAM_NAME, defaultValue);
        ContainerManager.autowireComponent(this);
    }

    static class Interpreter implements SearchQueryInterpreter
    {
        private boolean shouldValidate;
        private LabelManager labelManager;

        public void setShouldValidate(boolean shouldValidate)
        {
            this.shouldValidate = shouldValidate;
        }

        public void setLabelManager(LabelManager labelManager)
        {
            this.labelManager = labelManager;
        }

        public SearchQuery createSearchQuery(String value) throws SearchQueryInterpreterException
        {
            /*if (shouldValidate)
            {
                if (labelManager.getLabel(value) == null)
                {
                    throw new SearchQueryInterpreterException("'" + value + "' is not an existing label");
                }
            }*/

            /**
             * CONF-15398. We want to report invalid labels even if validation is suppressed using shouldValidate = false,
             * as you cannot make a valid search query out of an invalid label.
             *
             * Ideally this validation should performed on the value passed to createSearchQuery() before it is invoked.
             */
            if (LabelParser.parse(value) == null)
                throw new SearchQueryInterpreterException("'" + value + "' is an invalid label.");

            return new LabelQuery(value);
        }

    }

    @Override
    protected SearchQueryInterpreter createSearchQueryInterpreter(MacroExecutionContext ctx)
    {
        Interpreter interpreter = new Interpreter();
        interpreter.setShouldValidate(shouldValidate);
        interpreter.setLabelManager(labelManager);
        return interpreter;
    }

    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }
}

