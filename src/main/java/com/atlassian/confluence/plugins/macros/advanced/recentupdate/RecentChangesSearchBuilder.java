package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.params.ParameterException;
import com.atlassian.confluence.macro.query.BooleanQueryFactory;
import com.atlassian.confluence.macro.query.params.AuthorParameter;
import com.atlassian.confluence.macro.query.params.ContentTypeParameter;
import com.atlassian.confluence.macro.query.params.LabelParameter;
import com.atlassian.confluence.macro.query.params.SpaceKeyParameter;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.DefaultSearchWithToken;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.DefaultSearch;
import com.atlassian.confluence.search.v2.SearchFilter;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchWithToken;
import com.atlassian.confluence.search.v2.query.AllQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.searchfilter.LastModifierSearchFilter;
import com.atlassian.confluence.search.v2.searchfilter.SearchResultTypeSearchFilter;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.ModifiedSort;
import com.atlassian.confluence.util.GeneralUtil;
import com.google.common.base.Preconditions;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.Set;

/**
 * Builder for searches and search URLs.
 */
public class RecentChangesSearchBuilder
{
    public static int DEFAULT_SIZE = 15;

    /**
     * Comma-delimited list of labels. Labels can be prefixed with + or - to indicate
     * required or excluded.
     */
    private String labels;

    /**
     * Comma-delimited list of authors.
     */
    private String authors;
    private String spaceKeys;

    /**
     * Comma-delimited list of content types (e.g. {@link com.atlassian.confluence.pages.Page#CONTENT_TYPE}.
     * Types can be prefixed with + or - to indicate required or excluded.
     */
    private String contentTypes;

    private int startIndex = 0;
    private int pageSize = DEFAULT_SIZE;
    private long searchToken = -1;

    public ISearch buildSearch()
    {
        BooleanQueryFactory booleanQueryFactory;
        SearchFilter searchFilter = SiteSearchPermissionsSearchFilter.getInstance();

        try
        {
            booleanQueryFactory = new BooleanQueryFactory();

            if (StringUtils.isNotBlank(labels))
                booleanQueryFactory.addMust(getLabelQuery(labels));

            booleanQueryFactory.addMust(getContentTypeQuery(contentTypes));

            if (StringUtils.isNotBlank(authors))
            {
                SearchFilter authorFilter = getAuthorQuery(authors);
                if (authorFilter != null)
                    searchFilter = searchFilter.and(authorFilter);
            }

            if (StringUtils.isNotBlank(spaceKeys))
                booleanQueryFactory.addMust(getSpaceQuery(spaceKeys));
        }
        catch (ParameterException e) // thrown by any one of the getXXXXQuery() methods if they fail to build the query due to a badly-formed/invalid parameter value
        {
            throw new IllegalArgumentException(e);
        }

        return new DefaultSearch(booleanQueryFactory.toBooleanQuery(), ModifiedSort.DEFAULT, SearchResultTypeSearchFilter.CHANGES.and(searchFilter), startIndex, pageSize);
    }

    public SearchWithToken buildSearchWithToken()
    {
        Preconditions.checkArgument(searchToken > 0, "searchToken must be greater than 0.");

        return new DefaultSearchWithToken(buildSearch(), searchToken);
    }

    private SearchQuery getSpaceQuery(String spaceKeys) throws ParameterException
    {
        return new SpaceKeyParameter().findValue(newMacroExecutionContext("spaces", spaceKeys)).toBooleanQuery();
    }

    private SearchQuery getContentTypeQuery(String contentType) throws ParameterException
    {
        if (StringUtils.isBlank(contentType))
        {
            BooleanQueryFactory booleanQueryFactory = new BooleanQueryFactory();
            booleanQueryFactory.addMust(AllQuery.getInstance());
            booleanQueryFactory.addMustNot(new ContentTypeQuery(ContentTypeEnum.MAIL));
            return booleanQueryFactory.toBooleanQuery();
        }

        return new ContentTypeParameter().findValue(newMacroExecutionContext("type", contentType)).toBooleanQuery();
    }

    private SearchQuery getLabelQuery(String labels) throws ParameterException
    {
        return new LabelParameter().findValue(newMacroExecutionContext("labels", labels)).toBooleanQuery();
    }

    private SearchFilter getAuthorQuery(String authorsParamValue) throws ParameterException
    {
        final Set<String> authors = new AuthorParameter().findValue(newMacroExecutionContext("author", authorsParamValue));
        if (!authors.isEmpty())
            return new LastModifierSearchFilter(authors.toArray(new String[authors.size()]));
        else
            return null;
    }

    /**
     * Remove this when we decouple {@link com.atlassian.confluence.macro.params.Parameter} from {@link com.atlassian.confluence.macro.MacroExecutionContext}.
     */
    private MacroExecutionContext newMacroExecutionContext(String key, String value)
    {
        return new MacroExecutionContext(Collections.singletonMap(key, value), null, new PageContext());
    }

    public String buildSearchUrl(Theme theme, String contextPath)
    {
        StringBuilder url = new StringBuilder(contextPath == null ? "" : contextPath);

        url.append("/plugins/recently-updated/changes.action");
        url.append("?theme=").append(theme == null ? Theme.concise.name() : theme.name());

        if (pageSize != DEFAULT_SIZE)
            url.append("&pageSize=").append(pageSize);

        if (startIndex > 0)
            url.append("&startIndex=").append(startIndex);

        if (searchToken > 0)
            url.append("&searchToken=").append(searchToken);

        if (StringUtils.isNotBlank(authors))
            url.append("&authors=").append(GeneralUtil.urlEncode(authors));

        if (StringUtils.isNotBlank(labels))
            url.append("&labels=").append(GeneralUtil.urlEncode(labels));

        if (StringUtils.isNotBlank(spaceKeys))
            url.append("&spaceKeys=").append(GeneralUtil.urlEncode(spaceKeys));

        if (StringUtils.isNotBlank(contentTypes))
            url.append("&contentType=").append(contentTypes);

        return url.toString();
    }

    public RecentChangesSearchBuilder withLabels(String labels)
    {
        this.labels = labels;
        return this;
    }

    public RecentChangesSearchBuilder withAuthors(String authors)
    {
        this.authors = authors;
        return this;
    }

    public RecentChangesSearchBuilder withSpaceKeys(String spaceKeys)
    {
        this.spaceKeys = spaceKeys;
        return this;
    }

    public RecentChangesSearchBuilder withContentTypes(String contentTypes)
    {
        this.contentTypes = contentTypes;
        return this;
    }

    public RecentChangesSearchBuilder withStartIndex(int startIndex)
    {
        Preconditions.checkArgument(startIndex >= 0);

        this.startIndex = startIndex;
        return this;
    }

    public RecentChangesSearchBuilder withPageSize(int pageSize)
    {
        Preconditions.checkArgument(pageSize > 0);

        this.pageSize = pageSize;
        return this;
    }

    public RecentChangesSearchBuilder withSearchToken(long searchToken)
    {
        Preconditions.checkArgument(searchToken > 0);

        this.searchToken = searchToken;
        return this;
    }
}
