package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLOutputFactory;

import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactoryFactoryBean;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.macros.advanced.xhtml.deprecated.HTMLParagraphStripper;

/**
 * <p>Designate a certain part of a page to be the "excerpt". The body contents of the macro will now be
 * rendered to ensure consistency between page that has an excerpt and also other macros that include the
 * excerpt content. (CONF-6342)
 * <p/>
 * <p>Other macros that include
 * the excerpt (such as the excerpt-include) will need to be render it with the correct context so that things
 * like links, images etc.. are rendered and displayed correctly.
 */
public class ExcerptMacro implements Macro
{
    protected static Pattern stripPattern = Pattern.compile("<p>(.*?)</p>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
	    if(StringUtils.isNotEmpty(body))
	    {
    	    Matcher matcher = stripPattern.matcher(body);
    	    if(matcher.matches())
    	    {
    	        body = matcher.group(1);
    	    }
	    }
	    return shouldHideExcerpt(parameters) ? "" : body;
    }

    private boolean shouldHideExcerpt(Map parameters)
    {
        return "true".equalsIgnoreCase((String) parameters.get("hidden"));
    }

    public BodyType getBodyType()
    {
        return BodyType.RICH_TEXT;
    }

    public OutputType getOutputType()
    {
        return OutputType.INLINE;
    }
}
