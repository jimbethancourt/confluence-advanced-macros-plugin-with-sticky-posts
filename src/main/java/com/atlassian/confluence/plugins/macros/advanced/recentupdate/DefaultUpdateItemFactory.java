package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.userstatus.InlineWikiStyleRenderer;
import com.atlassian.confluence.userstatus.UserStatus;
import com.atlassian.confluence.util.actions.ContentTypesDisplayMapper;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.spring.container.ContainerManager;

public class DefaultUpdateItemFactory implements UpdateItemFactory
{
    private final DateFormatter dateFormatter;
    private final ContentTypesDisplayMapper contentTypesDisplayMapper;
    private final I18NBean i18n;

    public DefaultUpdateItemFactory(DateFormatter dateFormatter, I18NBean i18n, ContentTypesDisplayMapper contentTypesDisplayMapper)
    {
        if (i18n == null)
            throw new IllegalArgumentException("i18n is required.");

        this.contentTypesDisplayMapper = contentTypesDisplayMapper;
        this.dateFormatter = dateFormatter;
        this.i18n = i18n;
    }

    private String getIconClassForSearchResult(SearchResult searchResult)
    {
        return contentTypesDisplayMapper.getClassName(searchResult);
    }
    
    public UpdateItem get(SearchResult searchResult)
    {
        UpdateItem result = null;
        final String type = searchResult.getType();
        AnyTypeDao anyTypeDao = (AnyTypeDao) ContainerManager.getComponent("anyTypeDao");
        InlineWikiStyleRenderer inlineWikiStyleRenderer = (InlineWikiStyleRenderer) ContainerManager.getComponent("inlineWikiStyleRenderer");

        if (UserStatus.CONTENT_TYPE.equals(type))
            result = new UserStatusUpdateItem(searchResult, dateFormatter, i18n, inlineWikiStyleRenderer, getIconClassForSearchResult(searchResult));
        else if (Comment.CONTENT_TYPE.equals(type))
            result = new CommentUpdateItem(searchResult, dateFormatter, i18n, getIconClassForSearchResult(searchResult));
        else if (Attachment.CONTENT_TYPE.equals(type))
        {
            Attachment attachment = (Attachment) anyTypeDao.findByHandle(searchResult.getHandle());

            if (attachment != null)
            {
                ContentEntityObject content = attachment.getContent();

                if (content instanceof PersonalInformation)
                    result = new ProfilePictureUpdateItem(searchResult, dateFormatter, i18n, getIconClassForSearchResult(searchResult));
                else
                    result = new AttachmentUpdateItem(searchResult, dateFormatter, i18n, getIconClassForSearchResult(searchResult));
            }
        }
        else if (PersonalInformation.CONTENT_TYPE.equals(type))
        {
            int version = UpdateItemUtils.getContentVersion(searchResult);
            if (version > 1) // version 1 profiles can be created by the administrator when they create users from the admin interface (so ignore)
                result = new ProfileUpdateItem(searchResult, dateFormatter, i18n, getIconClassForSearchResult(searchResult));
        }
        else
            result = new ContentUpdateItem(searchResult, dateFormatter, i18n, getIconClassForSearchResult(searchResult));

        return result;
    }
}
