package com.atlassian.confluence.plugins.macros.advanced;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.ContentFilteringMacro;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.params.ParameterException;
import com.atlassian.confluence.macro.query.BooleanQueryFactory;
import com.atlassian.confluence.macro.query.InclusionCriteria;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchFieldNames;
import com.atlassian.confluence.search.v2.SearchFilter;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.CreatorQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.searchfilter.ContentPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.searchfilter.SpacePermissionsSearchFilter;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.Spaced;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.base.Joiner;

public class LabelledContentMacro extends ContentFilteringMacro
{
    private static final Logger log = Logger.getLogger(LabelledContentMacro.class);
    private static final String TEMPLATE_NAME = "com/atlassian/confluence/plugins/macros/advanced/labelledcontent.vm";

    private static final String SPACE_ALL = "@all";
    private static final String SPACE_SELF = "@self";
    private static final String WHERE_PARAM = "where";
    private static final String SPACE_ALL_KEYWORD = "conf_all";

    private static final String OPERATOR = "operator";
    private static final String OPERATOR_OR = "OR";
    private static final String OPERATOR_AND = "AND";

    private static final String DELIMITERS_REGEX = "[, ]+";
    private static final String QUERY_STRING_PARAM = "queryString";

    private static final String[] LABEL_KEYS = {"labels", "label", ""};
    private static final String LABEL_PREFIX = "labelText";

    private static final String[] SPACES = {"spaces", "space"};
    private static final String SPACE_PREFIX = "spacekey";

    private static final String[] TYPE_KEYS = {"type"};
    private static final String TYPE_PREFIX = "type";

    private ExcerptHelper excerptHelper;
    private Renderer viewRenderer;
    private ConfluenceActionSupport confluenceActionSupport;
    private WebResourceManager webResourceManager;

    /**
     * Helper class exposed to the macro's velocity context in order to simplify the templating code required to
     * display a page's excerpt.
     */
    public class ExcerptRendererHelper
    {
        private final ExcerptHelper helper;
        private final Renderer renderer;
        private final boolean showExcerpts;

        private ExcerptRendererHelper(boolean showExcerpts, ExcerptHelper helper, Renderer renderer)
        {
            this.helper = helper;
            this.renderer = renderer;
            this.showExcerpts = showExcerpts;
        }

        public boolean shouldRenderExcerpt(ContentEntityObject entity)
        {
            return showExcerpts && StringUtils.isNotBlank(helper.getExcerpt(entity));
        }

        public String getRenderedExcerpt(ContentEntityObject entity)
        {
            return renderer.render(helper.getExcerpt(entity), new DefaultConversionContext(entity.toPageContext()));
        }
    }

    public LabelledContentMacro()
    {
        super();
        labelParam.setValidate(false); // CONF-14235 - this macro shouldn't try to validate labels
    }

    public boolean isInline()
    {
        return false; 
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    @Override
    public String execute(MacroExecutionContext ctx) throws MacroException
    {
        webResourceManager.requireResource("confluence.macros.advanced:content-by-label-resources");

        Map<String, Object> contextMap = getMacroVelocityContext();
        
        Map<String, String> parameters = ctx.getParams();
        String title = parameters.get("title");

        BooleanQueryFactory queryFactory = new BooleanQueryFactory();

        // CONF-12749 Limit the label links to a single space if we only search a single space
        boolean limitLabelLinksToSpace = false;

        // parse the space(s) parameter
        spaceKeyParam.addParameterAlias("key");
        // CONF-14023 - default to searching all spaces
        spaceKeyParam.setDefaultValue(SPACE_ALL);
        BooleanQueryFactory spaceKeyQuery = spaceKeyParam.findValue(ctx);
        if (spaceKeyQuery != null)
        {
            queryFactory.addMust(spaceKeyQuery.toBooleanQuery());

            limitLabelLinksToSpace = isSingleSpace(spaceKeyQuery);
        }

        // use the "labels" parameter from ContentFilteringMacro
        labelParam.addParameterAlias("0");
        if (parameters.containsKey(OPERATOR) && parameters.get(OPERATOR).equalsIgnoreCase(OPERATOR_AND))
        {
            labelParam.setDefaultInclusionCriteria(InclusionCriteria.ALL);
        }
        else
        {
            labelParam.setDefaultInclusionCriteria(InclusionCriteria.ANY);
        }
        BooleanQueryFactory labelQuery = labelParam.findValue(ctx);
        if (labelQuery != null)
        {
            queryFactory.addMust(labelQuery.toBooleanQuery());
        }
        else
        {
            throw new MacroException(
                    getConfluenceActionSupport().getText("contentbylabel.error.label-parameter-required")
            );
        }

        List<String> labelNames = new ArrayList<String>();

        // we need a list of label names to display in case no content comes back.
        // For now, we're going to just stick the label string into a single-element list.
        labelNames.add(labelParam.getParameterValue(parameters));
        contextMap.put("labelNames", labelNames);

        // handle the other parameters.
        SubsetResultFilter subsetResultFilter;
        maxResultsParam.addParameterAlias("maxResults");
        maxResultsParam.setDefaultValue(ContentFilteringMacro.DEFAULT_MAX_RESULTS);
        try
        {
            Integer maxResults = maxResultsParam.findValue(ctx);
            contextMap.put("maxResults", maxResults);
            subsetResultFilter = new SubsetResultFilter(maxResults);
        }
        catch (ParameterException pe)
        {
            throw new MacroException(
                    getConfluenceActionSupport().getText("contentbylabel.error.parse-max-labels-param"),
                    pe);
        }

        SearchSort searchSort;
        try
        {
            searchSort = sortParam.findValue(ctx);
        }
        catch (ParameterException pe)
        {
            throw new MacroException(
                    getConfluenceActionSupport().getText("contentbylabel.error.parse-reverse-or-sort-param"),
                    pe);
        }

        try
        {
            BooleanQueryFactory contentTypeQuery = contentTypeParam.findValue(ctx);
            if (contentTypeQuery != null)
            {
                queryFactory.addMust(contentTypeQuery.toBooleanQuery());
            }
        }
        catch (ParameterException pe)
        {
            throw new MacroException(
                    getConfluenceActionSupport().getText(
                            "contentbylabel.error.parse-types-param",
                            new String[] { StringEscapeUtils.escapeHtml(pe.getMessage()) }
                    ), 
                    pe
            );
        }

        // parse author parameter
        Set<String> authors = authorParam.findValue(ctx);

        if (!authors.isEmpty())
        {
            BooleanQueryFactory authorQueryFactory = new BooleanQueryFactory();

            for (String author : authors)
            {
                authorQueryFactory.addShould(new CreatorQuery(author));
            }

            queryFactory.addMust(authorQueryFactory.toBooleanQuery());
        }


        BooleanQuery query = queryFactory.toBooleanQuery();
        final SearchFilter searchFilter = ContentPermissionsSearchFilter.getInstance().and(SpacePermissionsSearchFilter.getInstance());
        ISearch search = new ContentSearch(query, searchSort, searchFilter, subsetResultFilter);
        SearchResults searchResults;
        try
        {
            searchResults = searchManager.search(search, SearchFieldNames.createWithDefaultValues(null));
        }
        catch (InvalidSearchException ise)
        {
            throw new MacroException(getConfluenceActionSupport().getText("contentbylabel.error.run-search"), ise);
        }

        List<Searchable> contents = searchManager.convertToEntities(searchResults, true);

        contextMap.put("title", title);
        contextMap.put("contents", contents);
        contextMap.put("unfilteredResultsCnt", searchResults.getUnfilteredResultsCount());
        contextMap.put("showLabels", getBooleanParameter(parameters.get("showLabels"), true));
        contextMap.put("showSpace", getBooleanParameter(parameters.get("showSpace"), true));
        contextMap.put("limitLabelLinksToSpace", limitLabelLinksToSpace);
        contextMap.put("excerptHelper", new ExcerptRendererHelper(Boolean.valueOf(parameters.get("excerpt")), excerptHelper, viewRenderer));

        String searchPagesByLabelsURL = createSearchPagesByLabelsURL(getCurrentSpace(ctx.getPageContext().getEntity()), parameters);
        contextMap.put("searchPagesByLabelsURL", searchPagesByLabelsURL);
        return render(contextMap);
    }

    private String createSearchPagesByLabelsURL(Space currentSpace, Map<String, String> params)
    {
        SettingsManager settingsManager = ((SettingsManager) ContainerManager.getComponent("settingsManager"));
        StringBuilder queryString = new StringBuilder();
        queryString.append(settingsManager.getGlobalSettings().getBaseUrl() + "/dosearchsite.action?");
        
        // create query for space
        String spaceKey = "";
        String spaceQuery = null; // used when query in multiple spaces
        String[] spaceValues = getValues(params, SPACES);
        if (spaceValues == null)
        {
            spaceKey = SPACE_ALL_KEYWORD;
        }
        else
        {
            if (spaceValues.length == 1)
            {
                String spaceValue = spaceValues[0];
                spaceKey = SPACE_ALL.equals(spaceValue) ? SPACE_ALL_KEYWORD
                                                        : SPACE_SELF.equals(spaceValue) ? (currentSpace == null) ? SPACE_ALL_KEYWORD
                                                                                                                 : currentSpace.getKey()
                                                                                        : spaceValue;
            }
            else
            {
                // multiple spaces
                spaceKey = SPACE_ALL_KEYWORD;
                spaceQuery = createQuery(params, SPACES, SPACE_PREFIX, OPERATOR_OR);
            }
            
        }
        queryString.append(WHERE_PARAM + "=" + spaceKey);
        
        // create query string for other conditions
        queryString.append("&" + QUERY_STRING_PARAM + "=");
        String labelOperation = OPERATOR_OR;
        if (OPERATOR_AND.equals(params.get(OPERATOR)))
        {
            labelOperation = OPERATOR_AND;
        }
            
        //create queries for label, type
        String labelsQuery = createQuery(params, LABEL_KEYS, LABEL_PREFIX, labelOperation);
        String typesQuery = createQuery(params, TYPE_KEYS, TYPE_PREFIX, OPERATOR_OR);

        Joiner.on(" " + OPERATOR_AND + " ").skipNulls().appendTo(queryString, spaceQuery, labelsQuery, typesQuery);

        return queryString.toString();
    }

    private Space getCurrentSpace(ContentEntityObject ceo)
    {
        Space currentSpace = null;
        if (ceo instanceof Spaced)
        {
            currentSpace = ((Spaced) ceo).getSpace();
        }
        return currentSpace;
    }

    private String[] getValues(Map<String, String> params, String[] keys) {
        for (String key : keys)
        {
            if (params.containsKey(key)) {
                return params.get(key).split(DELIMITERS_REGEX);
            }
        }
        return null;
    }
    private String createQuery(Map<String, String> params, String[] keys, String prefix, String operation)
    {
        String[] values = getValues(params, keys);
        
        if (values != null)
        {
            String query = "(";
            for (String value : values)
            {
                query += prefix + ":" + value + " " + operation + " ";
            }
            query = query.substring(0, query.lastIndexOf(operation) - 1);
            query += ")";
            return query;
        }
        return null;
    }

    /* We assume that the query applies to a single space if there's 
     * only one component and it is an InSpace query type.
     * 
     * TODO  This probably belongs in the SpaceQuery class
     */
    private boolean isSingleSpace(BooleanQueryFactory spaceKeyQuery)
    {
        boolean limitLabelLinksToSpace = false;
        
        List params = spaceKeyQuery.toBooleanQuery().getParameters();
        if(params.size() == 1 && (params.get(0) instanceof InSpaceQuery) )
        {
            limitLabelLinksToSpace = true;
        }
        return limitLabelLinksToSpace;
    }

/// CLOVER:OFF
    @SuppressWarnings("unchecked")
    protected Map<String, Object> getMacroVelocityContext()
    {
        // current MacroUtils returns untyped map, but its keys are always
        // strings and we don't want to restrict what it can hold        
        return MacroUtils.defaultVelocityContext();
    }

    protected String render(Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate(TEMPLATE_NAME, contextMap);
    }
///CLOVER:ON
    
    private Boolean getBooleanParameter(String booleanValue, boolean defaultValue)
    {
        if (StringUtils.isNotBlank(booleanValue))
            return Boolean.valueOf(booleanValue);
        else
            return defaultValue;
    }

    public void setViewRenderer(Renderer viewRenderer)
    {
        this.viewRenderer = viewRenderer;
    }

    public void setExcerptHelper(ExcerptHelper excerptHelper)
    {
        this.excerptHelper = excerptHelper;
    }

    protected ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        return confluenceActionSupport;
    }

    public void setWebResourceManager(WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }
}
