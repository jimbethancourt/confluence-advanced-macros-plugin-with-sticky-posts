package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.links.linktypes.AbstractPageLink;
import com.atlassian.confluence.links.linktypes.BlogPostLink;
import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.v2.RenderUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static org.apache.commons.lang.StringUtils.isBlank;

// TODOXHTML CONFDEV-1221 remove dependency on renderer.v2

public class ExcerptIncludeMacro implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(ExcerptIncludeMacro.class);

    private PermissionManager permissionManager;
    private ExcerptHelper excerptHelper;
    private Renderer viewRenderer;
    private LinkResolver linkResolver;
    private I18NBeanFactory i18NBeanFactory;
    private LocaleManager localeManager;

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        log.debug("Beginning execute.");
        PageContext context = conversionContext.getPageContext();

        String linkText = getLinkText(parameters);
        AbstractPageLink link = getAbstractPageLink(context, linkText);
        if (link == null)
            return getI18nBean().getText("excerptinclude.error.cannot-link-to",
                new String[]{StringEscapeUtils.escapeHtml(linkText)});

        String linkTitle = linkText;
        ContentEntityObject contentEntityObject = null;
        if (link != null)
        {
            contentEntityObject = link.getDestinationContent();
            linkTitle = link.getPageTitle();
        }

        String bodyContent = getBodyContent(contentEntityObject, linkTitle, context);

        if (Boolean.parseBoolean(parameters.get("nopanel")))
            return bodyContent;

        return MacroPanel.wrap(GeneralUtil.unescapeEntities(linkTitle), bodyContent, parameters, context);
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.INLINE;
    }

    private String getBodyContent(ContentEntityObject contentEntityObject, String linkTitle, PageContext context)
    {
        if (contentEntityObject == null || !permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, contentEntityObject))
            return getI18nBean().getText("excerptinclude.error.page-does-not-exists",
                new String[]{StringEscapeUtils.escapeHtml(linkTitle)});

        if (ContentIncludeStack.contains(contentEntityObject))
        {
            I18NBean i18nBean = getI18nBean();
            String message = i18nBean.getText("excerptinclude.error.recursive.message");
            String contents = i18nBean.getText("excerptinclude.error.recursive.contents", new String[]{GeneralUtil.htmlEncode(contentEntityObject.getTitle())});
            return RenderUtils.blockError(message, contents);
        }

        try
        {
            ContentIncludeStack.push(contentEntityObject);
            // we need to render the excerpt with the correct page context
            PageContext excerptContext = new PageContext(contentEntityObject, context);
            String excerpt = excerptHelper.getExcerpt(contentEntityObject);
            ConversionContext excerptConversionContext = new DefaultConversionContext(excerptContext);
            return viewRenderer.render(excerpt, excerptConversionContext);
        }
        finally
        {
            ContentIncludeStack.pop();
        }
	}

    private String getLinkText(Map<String, String> parameters)
    {
        String linkText = parameters.get("0");
        if (isBlank(linkText))
        {
            linkText = parameters.get("pageTitle");
            if (isBlank(linkText))
            {
                linkText = parameters.get("blogPost");
            }
        }

        return StringUtils.defaultString(linkText).trim();
    }

    private AbstractPageLink getAbstractPageLink(PageContext context, String linkText)
    {
        Link link = linkResolver.createLink(context, linkText);
        if (!(link instanceof PageLink || link instanceof BlogPostLink))
            return null;

        return (AbstractPageLink)link;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setExcerptHelper(ExcerptHelper excerptHelper)
    {
        this.excerptHelper = excerptHelper;
    }

    public void setViewRenderer(Renderer viewRenderer)
    {
        this.viewRenderer = viewRenderer;
    }

    public void setLinkResolver(LinkResolver linkResolver)
    {
        this.linkResolver = linkResolver;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    public void setLocaleManager(LocaleManager localeManager)
    {
        this.localeManager = localeManager;
    }

    private I18NBean getI18nBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }
}
