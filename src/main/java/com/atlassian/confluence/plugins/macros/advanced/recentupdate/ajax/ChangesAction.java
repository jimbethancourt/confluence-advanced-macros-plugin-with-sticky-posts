package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;

/**
 * Action responsible for dispatching to appropriate changes action.
 *
 * @see SocialThemeChangesAction
 * @see ConciseThemeChangesAction
 */
public class ChangesAction extends ConfluenceActionSupport
{
    private String theme;

    @Override
    public void validate()
    {
        try
        {
            Theme.valueOf(theme);
        }
        catch (IllegalArgumentException e)
        {
            addActionError(theme + " no supported.");
        }

        super.validate();
    }

    @Override
    public String execute() throws Exception
    {
        return theme;
    }

    public void setTheme(String theme)
    {
        this.theme = theme;
    }
}
