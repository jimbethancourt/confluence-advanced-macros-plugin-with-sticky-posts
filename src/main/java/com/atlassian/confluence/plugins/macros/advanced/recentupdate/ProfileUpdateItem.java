package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.util.i18n.I18NBean;

public class ProfileUpdateItem extends AbstractUpdateItem
{
    public ProfileUpdateItem(SearchResult searchResult, DateFormatter dateFormatter, I18NBean i18n, String iconClass)
    {
        super(searchResult, dateFormatter, i18n, iconClass);
    }

    ///CLOVER:OFF
    public String getDescriptionAndDateKey()
    {
        return "update.item.desc.profile";
    }

    public String getDescriptionAndAuthorKey()
    {
        return "update.item.desc.author.profile";
    }
    ///CLOVER:ON

}
