package com.atlassian.confluence.plugins.macros.advanced;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.bonnie.Handle;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.search.lucene.filter.ContentPermissionsFilter;
import com.atlassian.confluence.search.v2.SearchFilter;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.searchfilter.ContentPermissionsSearchFilter;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.PageContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.core.datetime.FriendlyDateFormatter;
import com.atlassian.confluence.core.datetime.RequestTimeThreadLocal;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.ContentFilteringMacro;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.macro.params.ParameterException;
import com.atlassian.confluence.macro.query.BooleanQueryFactory;
import com.atlassian.confluence.macro.query.InclusionCriteria;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.CreatorQuery;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.search.v2.query.LabelQuery;
import com.atlassian.confluence.search.v2.searchfilter.SpacePermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.CreatedSort;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.Spaced;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.util.i18n.Message;
import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.opensymphony.util.TextUtils;

/**
 * Show a list of blog posts in a page.
 */
public class BlogPostsMacro extends ContentFilteringMacro implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(BlogPostsMacro.class);

    private static final String TEMPLATE_BLOG_POST_TITLES =
            "com/atlassian/confluence/plugins/macros/advanced/blog-posts-titles.vm";

    private static final String TEMPLATE_BLOG_POST_TITLES_MOBILE =
            "com/atlassian/confluence/plugins/macros/advanced/blog-posts-titles-mobile.vm";

    private static final String TEMPLATE_BLOG_POST =
            "com/atlassian/confluence/plugins/macros/advanced/blog-posts.vm";

    private static String MATCH_LABELS = "match-labels";
    private static String MATCH_LABELS_ANY = "any";
    private static String MATCH_LABELS_ALL = "all";

    private static String CONTENT = "content";
    private static String CONTENT_EXCERPTS = "excerpts";
    private static String CONTENT_TITLES = "titles";
    // From tests, it appears that this is one of the old values
    private static String CONTENT_TITLES_ALIAS = "title";
    private static String CONTENT_ENTIRE = "entire";

    private static String TIME = "time";

    private static final int EXCERPT_LENGTH = 500;

    private I18NBeanFactory i18NBeanFactory;
    private LocaleManager localeManager;
    private LabelManager labelManager;
    private ExcerptHelper excerptHelper;
    private Renderer viewRenderer;
    private VelocityHelperService velocityHelperService;
    private FormatSettingsManager formatSettingsManager;
    private UserAccessor userAccessor;
    private PermissionManager permissionManager;
    private SettingsManager settingsManager;

    /* Preparing for ctro inject (v2 plugin) */
    public BlogPostsMacro(I18NBeanFactory i18NBeanFactory, LocaleManager localeManager, AnyTypeDao anyTypeDao, LabelManager labelManager,
                          ExcerptHelper excerptHelper, Renderer viewRenderer, VelocityHelperService velocityHelperService, 
                          FormatSettingsManager dateFormatter, UserAccessor userAccessor, PermissionManager permissionManager,
                          SettingsManager settingsManager)
    {
        setI18NBeanFactory(i18NBeanFactory);
        setLocaleManager(localeManager);
        setAnyTypeDao(anyTypeDao);
        setLabelManager(labelManager);
        setExcerptHelper(excerptHelper);
        setViewRenderer(viewRenderer);
        setVelocityHelperService(velocityHelperService);
        setFormatSettingsManager(formatSettingsManager);
        setUserAccessor(userAccessor);
        setPermissionManager(permissionManager);
        setSettingsManager(settingsManager);
    }

    public void setVelocityHelperService(VelocityHelperService velocityHelperService)
    {
        this.velocityHelperService = velocityHelperService;
    }

    @SuppressWarnings("unused")
    public BlogPostsMacro()
    {
        this(null, null, null, null, null, null, null, null, null, null, null);
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    public void setLocaleManager(LocaleManager localeManager)
    {
        this.localeManager = localeManager;
    }

    public void setAnyTypeDao(AnyTypeDao anyTypeDao)
    {
        this.anyTypeDao = anyTypeDao;
    }

    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }

    public void setExcerptHelper(ExcerptHelper excerptHelper)
    {
        this.excerptHelper = excerptHelper;
    }

    public void setViewRenderer(Renderer viewRenderer)
    {
        this.viewRenderer = viewRenderer;
    }

    public void setFormatSettingsManager(FormatSettingsManager formatSettingsManager)
    {
        this.formatSettingsManager = formatSettingsManager;
    }

    public void setUserAccessor(UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        try
        {
            return super.execute(parameters, body, conversionContext.getPageContext());
        }
        catch (MacroException e)
        {
            throw new MacroExecutionException(e.getMessage(), e);
        }
    }

    @Override
    protected String execute(MacroExecutionContext ctx) throws MacroException
    {
        PageContext pageContext = ctx.getPageContext();
        Map<String, String> parameters = ctx.getParams();

        boolean popRequired = false;

        BooleanQueryFactory query = new BooleanQueryFactory();
        // we're searching for blog posts here
        query.addMust(new ContentTypeQuery(ContentTypeEnum.BLOG));
        // default sort should be by when they were created
        SearchSort searchSort = new CreatedSort(SearchSort.Order.DESCENDING);

        try
        {
            ContentEntityObject ceo = pageContext.getEntity();

            if (ceo instanceof BlogPost)
            {
                BlogPost blogPost = (BlogPost) ceo;

                // If we are rendering a page that is in the stack (i.e. a blog post included in itself), just exit
                if (ContentIncludeStack.contains(blogPost))
                    throw new MacroException(
                            getText(
                                    "blogposts.error.already-included-page",
                                    StringEscapeUtils.escapeHtml(blogPost.getDisplayTitle())
                            )
                    );

                ContentIncludeStack.push(blogPost);
                popRequired = true;
            }

            String labelFilterMode = parameters.containsKey(MATCH_LABELS) ? parameters.get(MATCH_LABELS) : "";
            if (labelFilterMode.equalsIgnoreCase(MATCH_LABELS_ALL))
            {
                labelParam.setDefaultInclusionCriteria(InclusionCriteria.ALL);
            }
            else
            {
                labelParam.setDefaultInclusionCriteria(InclusionCriteria.ANY);
                if (labelFilterMode.length() > 0 && !labelFilterMode.equalsIgnoreCase(MATCH_LABELS_ANY))
                {
                    throw new MacroException(
                            getText(
                                    "blogposts.error.invalid-label-filter",
                                    MATCH_LABELS_ALL, MATCH_LABELS_ANY
                            )
                    );
                }
            }

            BooleanQueryFactory labelQuery = labelParam.findValue(ctx);
            if (labelQuery != null)
            {
                query.addMust(labelQuery.toBooleanQuery());
            }

            // Strip out the maximum number of posts
            int maxResults;
            maxResultsParam.addParameterAlias("0");
            maxResultsParam.setDefaultValue(ContentFilteringMacro.DEFAULT_MAX_RESULTS);
            try
            {
                maxResults = maxResultsParam.findValue(ctx);
            }
            catch (ParameterException pe)
            {
                throw new MacroException(
                        getText(
                                "blogposts.error.invalid-max-posts",
                                StringEscapeUtils.escapeHtml("max")
                        )
                );
            }

            // Look for a sorting parameter
            try
            {
                SearchSort paramSearchSort = sortParam.findValue(ctx);
                if (paramSearchSort != null)
                {
                    searchSort = paramSearchSort;
                }
            }
            catch (ParameterException pe)
            {
                throw new MacroException(getText("blogposts.error.parse-reverse-or-sort-param"), pe);
            }

            // Strip out the time
            String time = parameters.get(TIME);

            // If there's a time specified, parse it
            if (StringUtils.isNotBlank(time))
            {
                try
                {
                    // Returns the number of seconds this time period represents
                    long duration = DateUtils.getDuration(time);

                    Date now = new Date();
                    Date then = new Date(now.getTime() - duration * 1000L);

                    // search for any posts modified between then and now, inclusive
                    query.addMust(new DateRangeQuery(then, now, true, true,
                            DateRangeQuery.DateRangeQueryType.MODIFIED));
                }
                catch (InvalidDurationException e)
                {
                    throw new MacroException(
                            getText(
                                    "blogposts.error.invalid-time-format",
                                    StringEscapeUtils.escapeHtml(time)
                            )
                    );
                }
            }

            // parse the space(s) parameter
            spaceKeyParam.setDefaultValue(ContentFilteringMacro.DEFAULT_SPACE_KEY);
            BooleanQueryFactory spaceKeyQuery = spaceKeyParam.findValue(ctx);
            if (spaceKeyQuery != null)
            {
                query.addMust(spaceKeyQuery.toBooleanQuery());
            }

            // parse an author parameter
            Set<String> authors = authorParam.findValue(ctx);

            if (!authors.isEmpty())
            {
                BooleanQueryFactory authorQueryFactory = new BooleanQueryFactory();

                for (String author : authors)
                {
                    authorQueryFactory.addShould(new CreatorQuery(author));
                }

                query.addMust(authorQueryFactory.toBooleanQuery());
            }
            final SearchFilter searchFilter = SpacePermissionsSearchFilter.getInstance().and(ContentPermissionsSearchFilter.getInstance());
            ISearch search = new ContentSearch(query.toBooleanQuery(), searchSort, searchFilter, new SubsetResultFilter(maxResults));
            SearchResults searchResults = searchManager.search(search);

            List<BlogPost> blogPosts = findBlogPosts(searchResults);

            Map<String, Object> velocityContextMap = velocityHelperService.createDefaultVelocityContext();

            /**
             * Flag that determines whether we should display the:
             * - whole contents
             * - an excerpt
             * - or only the title
             * of the blogpost
             */
            String blogPostContentDisplayOption = parameters.get(CONTENT);

            if (ctx.getParams().containsKey(StickyLabelParameter.PARAM_NAME[0])
                    && ctx.getParams().get(StickyLabelParameter.PARAM_NAME[0]).startsWith("true")) {

                BooleanQuery bq = query.toBooleanQuery();
                BooleanQueryFactory stickyPosts = new BooleanQueryFactory();
                stickyPosts.addMust(bq.getMustQueries());
                stickyPosts.addShould(bq.getShouldQueries());
                stickyPosts.addMustNot(bq.getMustNotQueries());

                StickyLabelParameter stickyLabelParameter = new StickyLabelParameter();
                stickyLabelParameter.setValidate(true);
                BooleanQueryFactory stickyLabelQuery = stickyLabelParameter.findValue(ctx);
                if (stickyLabelQuery != null) {
                    stickyPosts.addMust(new LabelQuery("sticky"));
                }

                if (stickyLabelQuery != null) {
                    query.addMustNot(new LabelQuery("sticky"));
                }

                ISearch stickySearch = new ContentSearch(stickyPosts.toBooleanQuery(), searchSort, SpacePermissionsSearchFilter.getInstance(), new SubsetResultFilter(maxResults));
                SearchResults stickySearchResults = searchManager.search(stickySearch);

                List<BlogPost> stickyResults = new ArrayList<BlogPost>();
                for (SearchResult stickySearchResult : stickySearchResults) {
                    Handle handle = stickySearchResult.getHandle();
                    BlogPost blogPost = (BlogPost) anyTypeDao.findByHandle(handle);
                    stickyResults.add(blogPost);
                    blogPosts.remove(blogPost);
                }

                velocityContextMap.put("stickyPosts", toPostHtmlTuple(stickyResults, blogPostContentDisplayOption));
            }

            velocityContextMap.put("contentType", blogPostContentDisplayOption);
            velocityContextMap.put("posts", toPostHtmlTuple(blogPosts, blogPostContentDisplayOption));

            Space currentSpace = getCurrentSpace(ceo);
            if (currentSpace != null)
            {
                final boolean canCreateBlog = permissionManager.hasCreatePermission(getUser(), currentSpace, BlogPost.class);
                velocityContextMap.put("canCreateBlog", canCreateBlog);
                String createBlogPostURL = settingsManager.getGlobalSettings().getBaseUrl() + "/pages/createblogpost.action?spaceKey=" + currentSpace.getKey();
                velocityContextMap.put("createBlogPostURL", createBlogPostURL);
            }

            // ADVMACROS-275 - Mobile view support for Blog Post macro
            if (StringUtils.equals("mobile", pageContext.getOutputDeviceType()))
            {
                return velocityHelperService.getRenderedTemplate(TEMPLATE_BLOG_POST_TITLES_MOBILE, velocityContextMap);
            }
            else
            {
                if (CONTENT_EXCERPTS.equals(blogPostContentDisplayOption))
                {
                    return velocityHelperService.getRenderedTemplate(TEMPLATE_BLOG_POST, velocityContextMap);
                }
                else if (CONTENT_TITLES.equals(blogPostContentDisplayOption) || CONTENT_TITLES_ALIAS.equals(blogPostContentDisplayOption))
                {
                    return velocityHelperService.getRenderedTemplate(TEMPLATE_BLOG_POST_TITLES, velocityContextMap);
                }
                else
                {
                    // the default option should be entire
                    return velocityHelperService.getRenderedTemplate(TEMPLATE_BLOG_POST, velocityContextMap);
                }
            }
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
            throw new MacroException(e.getMessage(), e);
        }
        finally
        {
            if (popRequired)
                ContentIncludeStack.pop();
        }
    }

    /**
     * Look up the actual blog post objects from the data layer for all the searchResultHandles.
     */
    List<BlogPost> findBlogPosts(SearchResults searchResults)
    {
        return Lists.newArrayList(
                Collections2.transform(
                        searchManager.convertToEntities(searchResults, SearchManager.EntityVersionPolicy.LATEST_VERSION),
                        new Function<Searchable, BlogPost>()
                        {
                            @Override
                            public BlogPost apply(Searchable searchable)
                            {
                                return (BlogPost) searchable;
                            }
                        }
                )
        );
    }

    /**
     * Converts an array of label strings to label queries.
     * Ignores unparseable label strings and labels that don't exist.
     *
     * @param labelNames an array of label strings
     * @return A Set of LabelQueries
     */
    private Set<LabelQuery> convertLabelStringsToLabels(String[] labelNames)
    {
        Set<LabelQuery> result = new HashSet<LabelQuery>(labelNames.length);
        for (String labelName : labelNames)
        {
            labelName = labelName.trim();
            // We will also need the associated label object
            ParsedLabelName ref = LabelParser.parse(labelName);

            // Only continue if the label name is valid
            if (ref != null)
            {
                Label label = labelManager.getLabel(ref);

                // Add the label, provided it's valid
                if (label != null)
                {
                    result.add(new LabelQuery(label));
                }
            }
        }
        return result;
    }


    /**
     * Renders each of the blog posts, skipping the current page if it is included
     *
     * @param blogPosts   the List of BlogPost objects
     * @param contentType the type of content to include (titles, excerpts or entire)
     * @return List of PostHtmlTuples
     */
    public List toPostHtmlTuple(List<BlogPost> blogPosts, String contentType)
    {
        List<PostHtmlTuple> list = new ArrayList<PostHtmlTuple>(blogPosts.size());
        User currentUser = AuthenticatedUserThreadLocal.getUser();

        for (BlogPost post : blogPosts)
        {
            // Make sure when rendering that if it loops, the current page is at the top of the stack. That way, if the blog-posts macro is executed again, it will exit.
            ContentIncludeStack.push(post);

            try
            {
                String excerpt = getContent(post, contentType);
                String renderedHtml = viewRenderer.render(excerpt, new DefaultConversionContext(post.toPageContext()));

                // extract error text only
                Pattern htmlErrorString = Pattern.compile("<div class=\"error\">.*</div>");
                Matcher match = htmlErrorString.matcher(renderedHtml);

                list.add(new PostHtmlTuple(post, match.find() ? match.group(0) :renderedHtml, new DateFormatter(userAccessor.getConfluenceUserPreferences(currentUser).getTimeZone(), formatSettingsManager, localeManager)));
            }
            finally
            {
                ContentIncludeStack.pop();
            }
        }

        return list;
    }

    /**
     * Returns the content for a particular BlogPost, based on what content setting the user has specified (titles, excerpts or entire)
     *
     * @param post        the BlogPost
     * @param contentType the content type to be returned
     * @return the content of the BlogPost
     */
    private String getContent(BlogPost post, String contentType)
    {
        String excerpt;
        if (CONTENT_EXCERPTS.equals(contentType))
        {
            excerpt = excerptHelper.getExcerpt(post);

            // If page does not have an excerpt macro declared than calculate one manually from the whole body
            if (StringUtils.isBlank(excerpt))
            {
                excerpt = excerptHelper.getText(post.getBodyAsString());
                
                // CONFDEV-3633
                // the excerptHelper returns element contents. If the element contained a CDATA with further markup
                // (e.g. in the case of a code macro showing XML code) then the excerpt will appear to contain
                // markup. Encode it.
                if (excerpt.length() > EXCERPT_LENGTH)
                {
                    excerpt = GeneralUtil.htmlEncode(TextUtils.trimToEndingChar(excerpt, EXCERPT_LENGTH)) + "&hellip;";
                }
                else
                {
                    excerpt = GeneralUtil.htmlEncode(excerpt); 
                }
                
                return excerpt;
            }
        }
        else
        {
            excerpt = post.getBodyAsString();
        }

        return excerpt;
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public static class PostHtmlTuple
    {
        private BlogPost post;
        private String renderedHtml;
        private FriendlyDateFormatter friendlyDateFormatter;

        public PostHtmlTuple(BlogPost post, String renderedHtml, DateFormatter dateFormatter)
        {
            this.post = post;
            this.renderedHtml = renderedHtml;
            this.friendlyDateFormatter = new FriendlyDateFormatter(RequestTimeThreadLocal.getTimeOrNow(), dateFormatter);
        }

        @SuppressWarnings("unused")
        public Message getFormattedDate()
        {
            return friendlyDateFormatter.getFormatMessage(post.getCreationDate());
        }

        @SuppressWarnings("unused")
        public BlogPost getPost()
        {
            return post;
        }

        @SuppressWarnings("unused")
        public String getRenderedHtml()
        {
            return renderedHtml;
        }
    }
    
    private String getText(String i18nkey, Object ... args)
    {
        return getI18nBean().getText(i18nkey, args);
    }

    private I18NBean getI18nBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    private User getUser()
    {
        return AuthenticatedUserThreadLocal.get();
    }
    
    private Space getCurrentSpace(ContentEntityObject ceo)
    {
        Space currentSpace = null;
        if (ceo instanceof Spaced)
        {
            currentSpace = ((Spaced) ceo).getSpace();
        }
        return currentSpace;
    }
}
