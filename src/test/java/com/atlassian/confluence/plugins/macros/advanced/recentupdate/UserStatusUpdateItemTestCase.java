package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.userstatus.InlineWikiStyleRenderer;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserStatusUpdateItemTestCase extends AbstractTestCase
{
    @Mock InlineWikiStyleRenderer inlineWikiStyleRenderer;
    @Mock SearchResult searchResult;
    @Mock DateFormatter dateFormatter;
    private UserStatusUpdateItem userStatusUpdateItem;

    public void testTargetIsRendered() // this is to allow wiki links to be embedded into user status messages
    {
        final String currentUserStatus = "currently _writing_ tests";
        when(searchResult.getDisplayTitle()).thenReturn(currentUserStatus);

        userStatusUpdateItem.getLinkedUpdateTarget();
        
        verify(inlineWikiStyleRenderer).render(currentUserStatus);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        userStatusUpdateItem = new UserStatusUpdateItem(searchResult, dateFormatter, i18n, inlineWikiStyleRenderer, "");
    }
}
