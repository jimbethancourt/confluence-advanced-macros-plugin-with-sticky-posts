package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.bonnie.AnyTypeObjectDao;
import com.atlassian.bonnie.Handle;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.mockito.Mock;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

import static java.util.Collections.emptyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class BlogPostsMacroTestCase extends AbstractTestCase
{

    @Mock private I18NBeanFactory i18NBeanFactory;
    @Mock private LocaleManager localeManager;
    @Mock private AnyTypeDao anyTypeDao;
    @Mock private LabelManager labelManager   ;
    @Mock private ExcerptHelper excerptHelper ;
    @Mock private Renderer viewRenderer;
    @Mock private VelocityHelperService velocityHelperService ;
    @Mock private FormatSettingsManager dateFormatter ;
    @Mock private UserAccessor userAccessor;
    @Mock private SearchManager searchManager;
    @Mock private PermissionManager permissionManager;
    @Mock private SettingsManager settingsManager;
    private BlogPostsMacro macro;


    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        initMocks(this);
        macro = new BlogPostsMacro(i18NBeanFactory, localeManager, anyTypeDao, labelManager, excerptHelper, viewRenderer,
                                   velocityHelperService, dateFormatter, userAccessor, permissionManager, settingsManager);
        macro.setSearchManager(searchManager);
    }

    private Document getPluginDescriptorAsDocument() throws IOException, DocumentException
    {
        InputStream descriptorInput = null;

        try
        {
            descriptorInput = getClass().getClassLoader().getResourceAsStream("atlassian-plugin.xml");

            Document descriptorDoc = new SAXReader().read(descriptorInput);

            return descriptorDoc;
        }
        finally
        {
            IOUtils.closeQuietly(descriptorInput);
        }
    }

    private boolean isBlogPostMacroAlias(Node node)
    {
        Element element = (Element) node;
        return element.attribute("class").getValue().equals(BlogPostsMacro.class.getName());
    }

    public void testBlogPostAliases() throws Exception
    {
        Document descriptorDoc = getPluginDescriptorAsDocument();

        assertTrue(isBlogPostMacroAlias(descriptorDoc.selectSingleNode("/atlassian-plugin/macro[@key='blog-post']")));
        assertTrue(isBlogPostMacroAlias(descriptorDoc.selectSingleNode("/atlassian-plugin/macro[@key='blogposts']")));
        assertTrue(isBlogPostMacroAlias(descriptorDoc.selectSingleNode("/atlassian-plugin/macro[@key='blogpost']")));
        assertTrue(isBlogPostMacroAlias(descriptorDoc.selectSingleNode("/atlassian-plugin/macro[@key='blogs']")));
        assertTrue(isBlogPostMacroAlias(descriptorDoc.selectSingleNode("/atlassian-plugin/macro[@key='blog']")));
        assertTrue(isBlogPostMacroAlias(descriptorDoc.selectSingleNode("/atlassian-plugin/macro[@key='news']")));
    }

    public void testSearchReturningWrongType() throws Exception
    {
        SearchResults searchResults = mock(SearchResults.class);
        SearchResult searchResult = mock(SearchResult.class);
        when(searchResults.iterator()).thenReturn(Collections.singletonList(searchResult).iterator());
        Handle handle = mock(Handle.class);
        when(searchResult.getHandle()).thenReturn(handle);
        Page page = new Page();
        when(anyTypeDao.findByHandle(handle)).thenReturn(page);

        assertEquals(emptyList(), macro.findBlogPosts(searchResults));
    }

    public void testSearchReturningMissingObject() throws Exception
    {
        SearchResults searchResults = mock(SearchResults.class);
        SearchResult searchResult = mock(SearchResult.class);
        when(searchResults.iterator()).thenReturn(Collections.singletonList(searchResult).iterator());
        Handle handle = mock(Handle.class);
        when(searchResult.getHandle()).thenReturn(handle);
        when(anyTypeDao.findByHandle(handle)).thenReturn(null);

        assertEquals(emptyList(), macro.findBlogPosts(searchResults));
    }
}
