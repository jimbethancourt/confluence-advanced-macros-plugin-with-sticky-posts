package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.macros.advanced.IndexMacro.AlphabeticPageListing;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.WikiRendererContextKeys;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.User;
import com.opensymphony.webwork.ServletActionContext;

import junit.framework.TestCase;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import org.radeox.api.engine.context.RenderContext;
import org.radeox.macro.parameter.MacroParameter;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IndexMacroTestCase extends TestCase
{
    @Mock
    private MacroParameter macroParameter;
    @Mock
    private RenderContext renderContext;
    @Mock
    private PageContext pageContext;
    @Mock
    private HttpServletRequest request;
    @Mock
    private SettingsManager settingsManager;
    @Mock
    private SpaceManager spaceManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private PageManager pageManager;

    private Map<String, Object> contextMap, contextParams, expectedMap, actualMap;
    private List<Page> pageList;
    private Page pageApple, pageBear, pageCat, pageDigit, pageOthers;

    private String baseUrl;
    private String fakeOutput = "fakeOutput";
    private Settings globalSettings;
    private Space testSpace;
    private IndexMacro indexMacro;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        actualMap = new HashMap<String, Object>();
        expectedMap = new HashMap<String, Object>();
        contextMap = new HashMap<String, Object>();
        contextParams = new HashMap<String, Object>();
        contextParams.put(WikiRendererContextKeys.RENDER_CONTEXT, pageContext);

        ServletActionContext.setRequest(request);

        pageList = new ArrayList<Page>();

        pageApple = new Page();
        pageApple.setTitle("Apple");
        pageList.add(pageApple);

        pageBear = new Page();
        pageBear.setTitle("Bear");
        pageList.add(pageBear);

        pageCat = new Page();
        pageCat.setTitle("Cat");
        pageList.add(pageCat);

        pageDigit = new Page();
        pageDigit.setTitle("12345");
        pageList.add(pageDigit);

        pageOthers = new Page()
        {
            @Override
            public String getTitle()
            {
                return "!@#";
            }
        };
        pageList.add(pageOthers);

        expectedMap.put("A", "Apple");
        expectedMap.put("B", "Bear");
        expectedMap.put("C", "Cat");
        expectedMap.put("0-9", "12345");
        expectedMap.put("@", "!@#");

        testSpace = new Space("tst");

        when(macroParameter.getContext()).thenReturn(renderContext);
        when(renderContext.getParameters()).thenReturn(contextParams);
        when(pageContext.getSpaceKey()).thenReturn(testSpace.getKey());

        baseUrl = "http://localhost:1990/confluence";

        globalSettings = new Settings();
        globalSettings.setBaseUrl(baseUrl);
        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        indexMacro = new TestIndexMacro();

        when(spaceManager.getSpace(testSpace.getKey())).thenReturn(testSpace);
        when(pageManager.getPages(testSpace, true)).thenReturn(pageList);
        when(permissionManager.getPermittedEntities((User) anyObject(), eq(Permission.VIEW), (List) anyObject())).thenAnswer(
                new Answer<List>()
                {
                    public List answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return (List) invocationOnMock.getArguments()[2];
                    }
                }
        );
    }

    @Override
    protected void tearDown() throws Exception
    {
        ServletActionContext.setRequest(null);
        super.tearDown();
    }

    public void testWithoutServletRequest() throws IllegalArgumentException, IOException
    {
        ServletActionContext.setRequest(null);

        indexMacro = new TestIndexMacro()
        {
            @Override
            protected String getVelocityRenderedTemplate(Map<String, Object> contextMap)
            {
                assertEquals(baseUrl, contextMap.get("baseurl"));

                AlphabeticPageListing pageListing = (AlphabeticPageListing) contextMap.get("pages");

                actualMap = getPageListing(pageListing, "A", "B", "C", "0-9", "@");

                assertEquals(expectedMap, actualMap);

                return fakeOutput;
            }
        };

        assertEquals(fakeOutput, indexMacro.getHtml(macroParameter));
    }

    public void testWithServletRequest() throws IllegalArgumentException, IOException
    {
        final String contextPath = "/foobar";
        when(request.getContextPath()).thenReturn(contextPath);

        IndexMacro indexMacro = new TestIndexMacro()
        {
            protected String getVelocityRenderedTemplate(Map<String, Object> contextMap)
            {
                assertEquals(contextPath, contextMap.get("baseurl"));

                AlphabeticPageListing pageListing = (AlphabeticPageListing) contextMap.get("pages");

                actualMap = getPageListing(pageListing, "A", "B", "C", "0-9", "@");

                assertEquals(expectedMap, actualMap);

                return fakeOutput;
            }
        };

        when(request.getContextPath()).thenReturn(contextPath);
        verify(settingsManager, never()).getGlobalSettings();

        assertEquals(fakeOutput, indexMacro.getHtml(macroParameter));
    }
    
    public void testGetMacroName()
    {
        IndexMacro indexMacro = new TestIndexMacro();
        
        assertEquals("index", indexMacro.getName());
    }
    
    public void testGetDescription()
    {
        IndexMacro indexMacro = new TestIndexMacro();
        
        assertEquals("1: ignored", indexMacro.getParamDescription()[0]);
    }

    private Map<String, Object> getPageListing(AlphabeticPageListing pageListing, String... keys)
    {
        Map<String, Object> map = new HashMap<String, Object>();

        for (String key : keys)
            for (Page page : (Set<Page>) pageListing.getPages(key))
                map.put(key, page.getTitle());

        return map;
    }

    private class TestIndexMacro extends IndexMacro
    {
        private TestIndexMacro()
        {
            setSettingsManager(settingsManager);
            setSpaceManager(spaceManager);
            setPermissionManager(permissionManager);
            setPageManager(pageManager);
        }

        @Override
        protected Map<String, Object> getDefaultVelocityContext()
        {
            return contextMap;
        }
    }
}
