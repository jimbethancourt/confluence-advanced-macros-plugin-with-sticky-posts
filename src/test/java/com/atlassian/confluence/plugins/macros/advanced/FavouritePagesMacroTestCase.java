package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.mail.Mail;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FavouritePagesMacroTestCase extends TestCase
{
	private Map<String, Object> parameters, contextMap;
	private RenderContext renderContext;
	private LabelManager labelManager;
	private Label label;
	private ParsedLabelName refFavourite, refFavorite;
	private Page page;
	private BlogPost blogPost;
	private Mail mail;
	private Comment comment;
	private PermissionManager permissionManager;

	@Override
	protected void setUp() throws Exception
	{
		super.setUp();

		parameters = new HashMap<String, Object>();
		parameters.put("maxResults", "4");

		contextMap = new HashMap<String, Object>();
		label = mock(Label.class);

		refFavourite = LabelParser.parse("my:favourite");
		refFavorite = LabelParser.parse("my:favorite");

		labelManager = mock(LabelManager.class);
		permissionManager = mock(PermissionManager.class);

		page = new Page();
		page.setTitle("Test Page");
		page.setBodyAsString("Content in Page");

		blogPost = new BlogPost();
		blogPost.setTitle("BlogPost Test");
		blogPost.setBodyAsString("Content in BlogPost");

		mail = new Mail();
		mail.setTitle("Mail Test");
		mail.setBodyAsString("Mail Test Content");

		comment = new Comment();
		comment.setTitle("Comment Test");
		comment.setBodyAsString("Comment Test Content");
	}

	public void testFilterWhenResultsContainsNonPageAndBlogWithoutAnySorting() throws MacroException
	{
		FavouritePagesMacro favouritePagesMacro = new FavouritePagesMacro()
		{
			@Override
			protected Map<String, Object> getDefaultVelocityContext() {
				return contextMap;
			}

			@Override
			protected ParsedLabelName parseLabel(
					String labelName) {
				if (labelName.equals("my:favourite"))
					return refFavourite;
				else
					return refFavorite;
			}

			@Override
			protected String getRenderedTemplate(
					Map<String, Object> contextMap) {
				List contents = (List) contextMap.get("contents");

				assertEquals(4, contextMap.get("maxResults"));
				assertEquals("Test Page", ((Page) contents.get(0)).getTitle());
				assertEquals("Content in Page", ((Page) contents.get(0)).getBodyAsString());
				assertEquals("BlogPost Test", ((BlogPost) contents.get(1)).getTitle());
				assertEquals("Content in BlogPost", ((BlogPost) contents.get(1)).getBodyAsString());
				assertEquals(2, contextMap.size());

				return "FavouritePagesMacro";
			}
		};
		favouritePagesMacro.setLabelManager(labelManager);
		favouritePagesMacro.setPermissionManager(permissionManager);

		final List<Labelable> listContent = new ArrayList<Labelable>();
		listContent.add(page);
		listContent.add(blogPost);
		listContent.add(mail);
		listContent.add(comment);

		when(labelManager.getLabel(refFavourite)).thenReturn(label);
		when(labelManager.getCurrentContentForLabel(label)).thenAnswer(
                new Answer<List<Labelable>>()
                {
                    @Override
                    public List<Labelable> answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return listContent;
                    }
                }
        );
		when(permissionManager.getPermittedEntities(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, listContent)).thenReturn(listContent);

		assertEquals("FavouritePagesMacro", favouritePagesMacro.execute(parameters, "", renderContext));
	}

	public void testIsInline()
	{
	    FavouritePagesMacro favouritePagesMacro = new FavouritePagesMacro();

	    assertEquals(false, favouritePagesMacro.isInline());
	}

	public void testHasBody()
	{
	    FavouritePagesMacro favouritePagesMacro = new FavouritePagesMacro();

        assertEquals(false, favouritePagesMacro.hasBody());
	}

	public void testBodyRenderMode()
	{
	    FavouritePagesMacro favouritePagesMacro = new FavouritePagesMacro();

        assertEquals(RenderMode.NO_RENDER, favouritePagesMacro.getBodyRenderMode());
	}

	/* Commented out since there is no sort implementation in FavouritePagesMacro.java */
//	public void testSort() throws MacroException
//	{
//		FavouritePagesMacro favouritePagesMacro = new FavouritePagesMacro()
//		{
//
//			@Override
//			protected Map<String, Object> getDefaultVelocityContext() {
//				return contextMap;
//			}
//
//			@Override
//			protected ParsedLabelName parseLabel(
//					String labelName) {
//				if (labelName.equals("my:favourite"))
//					return refFavourite;
//				else
//					return refFavorite;
//			}
//
//			@Override
//			protected String getRenderedTemplate(
//					Map<String, Object> contextMap) {
//				List contents = (List) contextMap.get("contents");
//
//				assertEquals(new Integer(4), (Integer) contextMap.get("maxResults"));
//
//				assertEquals("Test Page", ((Page) contents.get(0)).getTitle());
//				assertEquals("Content in Page", ((Page) contents.get(0)).getContent());
//				assertEquals("BlogPost Test", ((Page) contents.get(1)).getTitle());
//				assertEquals("Content in BlogPost", ((Page) contents.get(1)).getContent());
//
//				return "FavouritePagesMacro";
//			}
//		};
//		favouritePagesMacro.setPermissionManager(permissionManager);
//
//		parameters.put("reverse", "true");
//		parameters.put("sort", "title");
//
//		Page pageApple = new Page();
//		pageApple.setTitle("apple");
//		pageApple.setContent("a content inside apple page");
//
//		Page pageCattle = new Page();
//		pageCattle.setTitle("cattle");
//		pageCattle.setContent("b content inside cattle page");
//
//		List listContent = new ArrayList();
//		listContent.add(page);
//		listContent.add(pageApple);
//
//		when(labelManager.getLabel(refFavourite)).thenReturn(label);
//		when(labelManager.getCurrentContentForLabel(label)).thenReturn(listContent);
//		when(permissionManager.getPermittedEntities(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, listContent)).thenReturn(listContent);
//
//		assertEquals("FavouritePagesMacro", favouritePagesMacro.execute(parameters, "", renderContext));
//	}
}
