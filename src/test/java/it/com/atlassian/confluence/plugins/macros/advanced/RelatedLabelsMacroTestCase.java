package it.com.atlassian.confluence.plugins.macros.advanced;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;

public class RelatedLabelsMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private static final String COMMON_LABEL = "commonlabel";
    private static final String RELATED_LABEL_1 = "related1";
    private static final String RELATED_LABEL_2 = "related2";
    private static final String RELATED_LABEL_3 = "related3";
    private static final String RELATED_LABEL_4 = "related4";

    private void assertConfluenceLabelPresent(String label, boolean present)
    {
        if (present)
            assertElementPresentByXPath("//div[@class='wiki-content']/ul[@class='relatedlabels-list']/li[@class='relatedlabel']/a[text()='" + label + "']");
        else
            assertElementNotPresentByXPath("//div[@class='wiki-content']/ul[@class='relatedlabels-list']/li[@class='relatedlabel']/a[text()='" + label + "']");
    }

    private void assertConfluenceLabelPresent(String label)
    {
        assertConfluenceLabelPresent(label, true);
    }

    private void assertLabelNotPresent(String label)
    {
        assertConfluenceLabelPresent(label, false);
    }

    public void testRelatedLabelsDefaultFromCurrentPageLabels()
    {
        createPage(
                TEST_SPACE_KEY,
                "pageWithRelatedLabels", StringUtils.EMPTY,
                Arrays.asList(
                        COMMON_LABEL, RELATED_LABEL_1, RELATED_LABEL_2
                )
        );

        long testPageId = createPage(
                TEST_SPACE_KEY,
                "testRelatedLabelsDefaultFromCurrentPageLabels",
                "{related-labels}",
                Arrays.asList(
                        COMMON_LABEL
                )
        );

        viewPageById(testPageId);

        assertConfluenceLabelPresent(RELATED_LABEL_1);
        assertConfluenceLabelPresent(RELATED_LABEL_2);
    }

    public void testRelatedLabelsFromOneExplicitLabel()
    {
        createPage(
                TEST_SPACE_KEY,
                "pageWithRelatedLabels", StringUtils.EMPTY,
                Arrays.asList(
                        COMMON_LABEL, RELATED_LABEL_1, RELATED_LABEL_2
                )
        );

        long testPageId = createPage(
                TEST_SPACE_KEY,
                "testRelatedLabelsFromExplicitLabels",
                "{related-labels:labels=" + COMMON_LABEL + "}"
        );

        viewPageById(testPageId);

        assertConfluenceLabelPresent(RELATED_LABEL_1);
        assertConfluenceLabelPresent(RELATED_LABEL_2);
        assertLabelNotPresent(COMMON_LABEL);
    }

    public void testRelatedLabelsFromMultipleExplicitLabels()
    {
        createPage(
                TEST_SPACE_KEY,
                "pageWithRelatedLabels", StringUtils.EMPTY,
                Arrays.asList(
                        RELATED_LABEL_3, RELATED_LABEL_4
                )
        );
        createPage(
                TEST_SPACE_KEY,
                "pageWithRelatedLabels2", StringUtils.EMPTY,
                Arrays.asList(
                        RELATED_LABEL_1, RELATED_LABEL_2
                )
        );

        long testPageId = createPage(
                TEST_SPACE_KEY,
                "testRelatedLabelsFromMultipleExplicitLabelsAreCombined",
                "{related-labels:labels=" + RELATED_LABEL_1 + "," + RELATED_LABEL_3 + "}"
        );

        viewPageById(testPageId);

        assertConfluenceLabelPresent(RELATED_LABEL_2);
        assertConfluenceLabelPresent(RELATED_LABEL_4);
        assertLabelNotPresent(RELATED_LABEL_1);
        assertLabelNotPresent(RELATED_LABEL_3);
    }

    public void testExplicitLabelIncludedInResultsIfItIsOneRelatedLabel()
    {
        createPage(
                TEST_SPACE_KEY,
                "pageWithRelatedLabels", StringUtils.EMPTY,
                Arrays.asList(
                        RELATED_LABEL_3, RELATED_LABEL_4, RELATED_LABEL_1
                )
        );
        createPage(
                TEST_SPACE_KEY,
                "pageWithRelatedLabels2", StringUtils.EMPTY,
                Arrays.asList(
                        RELATED_LABEL_1, RELATED_LABEL_2
                )
        );

        long testPageId = createPage(
                TEST_SPACE_KEY,
                "testExplicitLabelIncludedInResultsIfItIsOneRelatedLabel",
                "{related-labels:labels=" + RELATED_LABEL_1 + "," + RELATED_LABEL_3 + "}"
        );

        viewPageById(testPageId);

        assertConfluenceLabelPresent(RELATED_LABEL_1);
        assertConfluenceLabelPresent(RELATED_LABEL_2);
        assertConfluenceLabelPresent(RELATED_LABEL_3);
        assertConfluenceLabelPresent(RELATED_LABEL_4);
    }

    public void testNameSpacedLabelsSupported()
    {
        String nameSpacedCommonLabel = "my:" + COMMON_LABEL;

        createPage(
                TEST_SPACE_KEY,
                "pageWithRelatedLabels", StringUtils.EMPTY,
                Arrays.asList(
                        nameSpacedCommonLabel, RELATED_LABEL_1, RELATED_LABEL_2
                )
        );

        long testPageId = createPage(
                TEST_SPACE_KEY,
                "testRelatedLabelsDefaultFromCurrentPageLabels",
                "{related-labels:labels=" + nameSpacedCommonLabel + "}",
                Arrays.asList(
                        nameSpacedCommonLabel
                )
        );

        viewPageById(testPageId);

        assertConfluenceLabelPresent(RELATED_LABEL_1);
        assertConfluenceLabelPresent(RELATED_LABEL_2);
    }
}
