package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.pages.BlogPost;

import java.util.Date;

public class IncludeMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    public void testIncludeExcerptMacro()
    {
        String otherPageTitle = "otherPageTitle";
        String otherPageContent = "otherPageContent";

        createPage(TEST_SPACE_KEY, otherPageTitle, "{excerpt}"+otherPageContent+"{excerpt}");
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeExcerptFromOtherPage", "{excerpt-include:" + otherPageTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(otherPageContent);
    }

    public void testIncludeContentFromOtherPage()
    {
        String otherPageTitle = "otherPageTitle";
        String otherPageContent = "otherPageContent";

        createPage(TEST_SPACE_KEY, otherPageTitle, otherPageContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromOtherPage", "{include:" + otherPageTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(otherPageContent);
    }

    public void testIncludeContentFromOtherPageInOtherSpace()
    {
        String otherSpaceKey = "osk";
        String otherPageTitle = "otherPageTitle";
        String otherPageContent = "otherPageContent";

        createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);

        createPage(otherSpaceKey, otherPageTitle, otherPageContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromOtherPageInOtherSpace", "{include:" + otherSpaceKey + ":" + otherPageTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(otherPageContent);
    }

    public void testIncludeContentFromBlogpost()
    {
        String blogTitle = "Test Blog Post";
        String blogContent = "This is content from a blog post!";
        Date now = new Date();

        createBlogPost(TEST_SPACE_KEY, blogTitle, blogContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromBlogpost",
                "{include:/" + BlogPost.toDatePath(now) + "/" + blogTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(blogContent);
    }

    public void testIncludeContentFromBlogpostInOtherSpace()
    {
        String otherSpaceKey = "osk";
        String blogTitle = "Test Blog Post";
        String blogContent = "This is content from a blog post!";
        Date now = new Date();

        createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);
        createBlogPost(otherSpaceKey, blogTitle, blogContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromBlogpostInOtherSpace",
                "{include:" + otherSpaceKey + ":/" + BlogPost.toDatePath(now) + "/" + blogTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(blogContent);
    }

    public void testIncludeRestrictedContentNotShown()
    {
        String otherPageTitle = "otherPageTitle";
        String otherPageContent = "otherPageContent";

        createPage(TEST_SPACE_KEY, otherPageTitle, otherPageContent);



        long testPageId = createPage("ds", "testIncludeRestrictedContentNotShown", "{include:" + TEST_SPACE_KEY + ":" + otherPageTitle + "}");
        String restrictedUser = "restricteduser";
        createUser(restrictedUser, restrictedUser, restrictedUser, new String[] { "confluence-users" });
        logout();

        login(restrictedUser, restrictedUser);

        viewPageById(testPageId);
        assertEquals(
                "Unable to render {include} The included page could not be found.",
                getElementTextByXPath("//div[@class='wiki-content']/div[@class='error']")
        );
    }

    public void testInvalidSpaceKeyErrorMessageHtmlEncoded()
    {
        viewPageById(createPage("ds", "testInvalidSpaceKeyErrorMessageHtmlEncoded", "{include:<script>alert(1)</script>:xx}"));
        assertEquals(
                "Unable to render {include} The included page could not be found.",
                getElementTextByXPath("//div[@class='wiki-content']/div[@class='error']"));
    }

    public void testIncludeContentFromOtherPageTileContainsSpecialCharacters()
    {
        String otherPageTitle = "test ^ & ! ? / title";
        String otherPageContent = "otherPageContent";

        createPage(TEST_SPACE_KEY, otherPageTitle, otherPageContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromOtherPage", "{include:" + otherPageTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(otherPageContent);
    }

    public void testIncludeContentFromOtherPageTileContainsColon()
    {
        String otherPageTitle = "test:title";
        String otherPageContent = "otherPageContent";

        createPage(TEST_SPACE_KEY, otherPageTitle, otherPageContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromOtherPage", "{include:" + TEST_SPACE_KEY+ ":" +otherPageTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(otherPageContent);
    }

    public void testIncludeContentFromOtherPageTileContainsSpecialCharacterAndOtherSpaceKey()
    {
        String otherSpaceKey = "osk";
        String otherPageTitle = "test : ^ & ! ? title";
        String otherPageContent = "otherPageContent";

        createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);

        createPage(otherSpaceKey, otherPageTitle, otherPageContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromOtherPageInOtherSpace", "{include:" + otherSpaceKey + ":" + otherPageTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(otherPageContent);
    }

    public void testIncludeContentFromBlogPostTitleContainsSpecialCharacters()
    {
        String blogTitle = "blogpost / ^ & ! ? title";
        String blogContent = "This is content from a blog post!";
        Date now = new Date();

        createBlogPost(TEST_SPACE_KEY, blogTitle, blogContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromBlogpost",
                "{include:/" + BlogPost.toDatePath(now) + "/" + blogTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(blogContent);
    }

    public void testIncludeContentFromBlogPostTitleContainsColon()
    {
        String blogTitle = "blogpost:title";
        String blogContent = "This is content from a blog post!";
        Date now = new Date();

        createBlogPost(TEST_SPACE_KEY, blogTitle, blogContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromBlogpost",
                "{include:" + TEST_SPACE_KEY + ":/" + BlogPost.toDatePath(now) + "/" + blogTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(blogContent);
    }

    public void testIncludeContentFromBlogpostContainsSpecialCharactersInOtherSpace()
    {
        String otherSpaceKey = "osk";
        String blogTitle = "blogpost / ^ & ! ? title";
        String blogContent = "This is content from a blog post!";
        Date now = new Date();

        createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);
        createBlogPost(otherSpaceKey, blogTitle, blogContent);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromBlogpostInOtherSpace",
                "{include:" + otherSpaceKey + ":/" + BlogPost.toDatePath(now) + "/" + blogTitle + "}");

        viewPageById(testPageId);
        assertTextPresent(blogContent);
    }
}
