package it.com.atlassian.confluence.plugins.macros.advanced;


import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import java.io.File;

import org.apache.commons.lang.StringUtils;

public class GalleryMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private File backupFile;

    private static final String GALLERY_MACRO_TEST_PAGE_TITLE = "Gallery Macro Test Page";

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        backupFile = restoreDataFromZipClassPathResource("gallery-macro-test-data.zip");
    }

    @Override
    protected void tearDown() throws Exception
    {
        try
        {
            backupFile.delete();
        }
        finally
        {
            super.tearDown();
        }
    }

    private long getGalleryMacroTestPageId()
    {
        PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle(GALLERY_MACRO_TEST_PAGE_TITLE);

        return pageHelper.findBySpaceKeyAndTitle();
    }

    private void editGalleryTestPage(String content)
    {
        updatePage(getGalleryMacroTestPageId(), content);
    }

    private void assertGalleryTitle(String title)
    {
        assertEquals(
                title,
                getElementTextByXPath("//div[@class='wiki-content']/div[@class='gallery']/table//tr/th/h4")
        );
    }

    public void testTitleRendered()
    {
        String title = "foo";
        editGalleryTestPage("{gallery:title=" + title + "}");
        viewPageById(getGalleryMacroTestPageId());

        assertGalleryTitle(title);
    }

    private void assertImagePresent(long testPageId, int rowIndex, int columnIndex, String fileName, String comment)
    {
        String contextPath = getElementAttributeByXPath("//meta[@id='confluence-context-path']", "content");

        assertElementPresentByXPath(
                "//div[@class='wiki-content']/div[@class='gallery']/table"
                        + "//tr[" + (rowIndex + 1) + "]/td[" + (columnIndex + 1) + "]/a[@href='" + contextPath + "/download/attachments/" + testPageId + "/" + fileName + "?api=v2']/img"
        );

        if (StringUtils.isNotBlank(comment))
        {
            assertEquals(
                        comment,
                        getElementTextByXPath(
                                "//div[@class='wiki-content']/div[@class='gallery']/table"
                                        + "//tr[" + (rowIndex + 2) + "]/td[" + (columnIndex + 1) + "]/div[@class='gallery-caption']"
                        )
            );
        }
    }

    public void testAttachmentCommentsShownInGallery()
    {
        editGalleryTestPage("{gallery:sort=size}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "small.png", "Small image.");
        assertImagePresent(testPageId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(testPageId, 0, 2, "big.png", "Big image.");
    }

    public void testRenderGalleryInCustomNumberOfColumns()
    {
        editGalleryTestPage("{gallery:columns=2}");
        viewPageById(getGalleryMacroTestPageId());

        assertElementPresentByXPath(
                "//div[@class='wiki-content']/div[@class='gallery']/table//tr/td/a[@class='gallery-link']/img"
        );
        assertElementPresentByXPath(
                "//div[@class='wiki-content']/div[@class='gallery']/table//tr/td[2]/a[@class='gallery-link']/img"
        );
        assertElementNotPresentByXPath(
                "//div[@class='wiki-content']/div[@class='gallery']/table//tr/td[3]"
        );
        assertElementPresentByXPath(
                "//div[@class='wiki-content']/div[@class='gallery']/table//tr[3]/td/a[@class='gallery-link']/img"
        );
    }

    public void testExcludeMultipleImagesFromGallery()
    {
        editGalleryTestPage("{gallery:exclude=small.png,big.png}");

        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "medium.png", "Medium sized image.");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='gallery']//td[@class='gallery-image'][2]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='gallery']//td[@class='gallery-caption'][2]");

    }

    public void testIncludeMultipleImagesFromGallery()
    {
        editGalleryTestPage("{gallery:include=small.png,big.png|sort=size}");

        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "small.png", "Small image.");
        assertImagePresent(testPageId, 0, 1, "big.png", "Big image.");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='gallery']//td[@class='gallery-image'][3]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='gallery']//td[@class='gallery-caption'][3]");

    }

    public void testImageIncludedOnlyIfNotExcluded()
    {
        editGalleryTestPage("{gallery:include=small.png,big.png|exclude=big.png|sort=size}");

        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "small.png", "Small image.");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='gallery']//td[@class='gallery-image'][2]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='gallery']//td[@class='gallery-caption'][2]");
    }

    public void testShowGalleryFromImagesOfOtherPageInSameSpace()
    {
        long pageWithImagesId = getGalleryMacroTestPageId();
        long testPageId = createPage(
                TEST_SPACE_KEY,
                "testShowGalleryFromImagesOfOtherPageInSameSpace",
                "{gallery:page=" + GALLERY_MACRO_TEST_PAGE_TITLE + "|sort=size}"
        );

        viewPageById(testPageId);


        assertImagePresent(pageWithImagesId, 0, 0, "small.png", "Small image.");
        assertImagePresent(pageWithImagesId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(pageWithImagesId, 0, 2, "big.png", "Big image.");
    }

    public void testShowGalleryFromImagesOfOtherPageInOtherSpace()
    {
        long pageWithImagesId = getGalleryMacroTestPageId();
        long testPageId = createPage(
                DEMO_SPACE_KEY,
                "testShowGalleryFromImagesOfOtherPageInOtherSpace",
                "{gallery:page=" + TEST_SPACE_KEY +  ":"  + GALLERY_MACRO_TEST_PAGE_TITLE + "|sort=size}"

        );

        viewPageById(testPageId);


        assertImagePresent(pageWithImagesId, 0, 0, "small.png", "Small image.");
        assertImagePresent(pageWithImagesId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(pageWithImagesId, 0, 2, "big.png", "Big image.");
    }

    public void testSortGalleryByFileName()
    {
        editGalleryTestPage("{gallery:sort=name}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "big.png", "Big image.");
        assertImagePresent(testPageId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(testPageId, 0, 2, "small.png", "Small image.");
    }

    public void testSortGalleryByFileNameReversedUsingReverseParameter()
    {
        editGalleryTestPage("{gallery:sort=name|reverse=true}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "small.png", "Small image.");
        assertImagePresent(testPageId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(testPageId, 0, 2, "big.png", "Big image.");
    }

    public void testSortGalleryByComment()
    {
        editGalleryTestPage("{gallery:sort=comment}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "big.png", "Big image.");
        assertImagePresent(testPageId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(testPageId, 0, 2, "small.png", "Small image.");
    }

    public void testSortGalleryByCommentReversedUsingReverseParameter()
    {
        editGalleryTestPage("{gallery:sort=comment|reverse=true}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "small.png", "Small image.");
        assertImagePresent(testPageId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(testPageId, 0, 2, "big.png", "Big image.");
    }

    public void testSortGalleryByLastModifiedDate()
    {
        editGalleryTestPage("{gallery:sort=date}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "medium.png", "Medium sized image.");
        assertImagePresent(testPageId, 0, 1, "big.png", "Big image.");
        assertImagePresent(testPageId, 0, 2, "small.png", "Small image.");
    }

    public void testSortGalleryByLastModifiedDateReversedUsingReverseParameter()
    {
        editGalleryTestPage("{gallery:sort=date|reverse=true}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "small.png", "Small image.");
        assertImagePresent(testPageId, 0, 1, "big.png", "Big image.");
        assertImagePresent(testPageId, 0, 2, "medium.png", "Medium sized image.");
    }

    public void testSortGalleryBySize()
    {
        editGalleryTestPage("{gallery:sort=size}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "small.png", "Small image.");
        assertImagePresent(testPageId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(testPageId, 0, 2, "big.png", "Big image.");
    }

    public void testSortGalleryBySizeReversedUsingReverseParameter()
    {
        editGalleryTestPage("{gallery:sort=size|reverse=true}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertImagePresent(testPageId, 0, 0, "big.png", "Big image.");
        assertImagePresent(testPageId, 0, 1, "medium.png", "Medium sized image.");
        assertImagePresent(testPageId, 0, 2, "small.png", "Small image.");
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-107">ADVMACROS-107</a>
     */
    public void testTitleHtmlEncoded()
    {
        String jsAsTitle = "<script>alert('hi')</script>";
        editGalleryTestPage("{gallery:title=" +jsAsTitle + "}");
        long testPageId = getGalleryMacroTestPageId();
        viewPageById(testPageId);

        assertEquals(jsAsTitle, getElementTextByXPath("//div[@class='wiki-content']//h4[@class='gallery-title']"));
    }
}
