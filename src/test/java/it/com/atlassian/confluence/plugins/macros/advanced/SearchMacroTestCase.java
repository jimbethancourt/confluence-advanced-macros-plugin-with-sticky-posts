package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.user.PersonalInformation;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;

import java.util.Date;
import java.io.UnsupportedEncodingException;

public class SearchMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private void assertSearchResultHeading(String resultCount, String queryString)
    {
        assertEquals(
                "Found " + resultCount + " search result(s) for " + queryString + ".",
                getElementTextByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/h4[@class='resultSummary']")
        );
    }

    public void testNoSearchResultReturnedMessageShownWhenQueryReturnsNoResult()
    {
        String queryString = "foo";
        long testPageId = createPage(TEST_SPACE_KEY,
                "testNoSearchResultReturnedMessageShownWhenQueryReturnsNoResult",
                "{search:query=" + queryString + "}"
        );

        viewPageById(testPageId);

        assertSearchResultHeading("0", queryString);
    }

    private void assertSearchResultContainsRow(String spaceKey, String contentType, String contentTitle, Date contentCreationDate)
    {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);
        assertTrue(spaceHelper.read());
        String spaceName = spaceHelper.getName();

        String dateString = FastDateFormat.getInstance("MMM dd, yyyy").format(contentCreationDate);

        String contentIconClass;

        if (StringUtils.equals(contentType, BlogPost.CONTENT_TYPE))
        {
            contentIconClass = "icon-blog";
            dateString += " - Blog";
        }
        else if (StringUtils.equals(contentType, SpaceDescription.CONTENT_TYPE_SPACEDESC))
        {
            contentIconClass = "icon-space";
        }
        else if (StringUtils.equals(contentType, Comment.CONTENT_TYPE))
        {
            contentIconClass = "icon-comment";
            dateString += " - Comments";
        }
        else if (StringUtils.equals(contentType, Attachment.CONTENT_TYPE))
        {
            contentIconClass = "icon-file-unknown";
            dateString = "Text File - 0.0 kB - " + dateString + " - Download - Attachments";
            assertEquals(
                    "Download",
                    getElementTextByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/div[@class='result']/a[text()='" + contentTitle + "']/../span[@class='smalltext'][2]/a")
            );
            assertEquals(
                    "Attachments",
                    getElementTextByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/div[@class='result']/a[text()='" + contentTitle + "']/../span[@class='smalltext'][2]/a[2]")
            );
        }
        else if (StringUtils.equals(contentType, PersonalInformation.CONTENT_TYPE))
        {
            contentIconClass = "icon-user";
        }
        else
        {
            contentIconClass = "icon-page";
        }

        assertElementPresentByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/div[@class='result']/a[text()='" + contentTitle + "']");

        /* Only do space name assertion if content type is not space description */
        if (!StringUtils.equals(SpaceDescription.CONTENT_TYPE_SPACEDESC, contentType)
            && !StringUtils.equals(PersonalInformation.CONTENT_TYPE, contentType))
        {

            assertElementPresentByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/div[@class='result']/a[text()='" + contentTitle + "']/../span/a[text()='" + spaceName + "']");

            assertEquals(
                    dateString,
                    getElementTextByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/div[@class='result']/a[text()='" + contentTitle + "']/../span[@class='smalltext'][2]")
            );
        }

        assertTrue(
                getElementAttributeByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/div[@class='result']/a[text()='" + contentTitle + "']/../span", "class").contains(contentIconClass)
        );
//        assertEquals(
//                spaceName,
//                getElementTextByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/div[@class='result']/a[text()='" + contentTitle + "']/../span/a")
//        );
    }

    public void testDefaultMaximumSeachResultIsTen()
    {
        for (int i = 0; i < 11; ++i)
            createPage(TEST_SPACE_KEY, "Foobar " + i, StringUtils.EMPTY);

        String queryString = "Foobar";
        long testPageId = createPage(TEST_SPACE_KEY, "testDefaultMaximumSeachResultIsTen",
                "{search:query=" + queryString + "}"
        );

        getIndexHelper().update();


        viewPageById(testPageId);

        assertSearchResultHeading("10", queryString);


        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][10]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][11]");
    }

    public void testCustomMaxLimitRespected()
    {
        for (int i = 0; i < 11; ++i)
            createPage(TEST_SPACE_KEY, "Foobar " + i, StringUtils.EMPTY);

        String queryString = "Foobar";
        String maxLimit = "5";
        long testPageId = createPage(TEST_SPACE_KEY, "testCustomMaxLimitRespected",
                "{search:query=" + queryString + "|maxLimit=" + maxLimit + "}"
        );

        getIndexHelper().update();


        viewPageById(testPageId);

        assertEquals(
                "Found " + maxLimit + " search result(s) for " + queryString + ".",
                getElementTextByXPath("//div[@class='wiki-content']//div[@class='searchMacro']/h4[@class='resultSummary']")
        );


        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][" + maxLimit + "]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][6]");
    }

    public void testSearchInOtherSpace()
    {
        deleteSpace(DEMO_SPACE_KEY);
        createSpace(DEMO_SPACE_KEY, DEMO_SPACE_KEY, DEMO_SPACE_KEY);

        createPage(DEMO_SPACE_KEY, "Foobar", StringUtils.EMPTY);

        String spaceKey = DEMO_SPACE_KEY;
        String queryString = "Foobar";
        long testPageId = createPage(TEST_SPACE_KEY, "testSearchInOtherSpace",
                "{search:query=" + queryString + "|spacekey=" + spaceKey + "}"
        );

        getIndexHelper().update();

        viewPageById(testPageId);
        assertSearchResultHeading("1", queryString);

        assertSearchResultContainsRow(DEMO_SPACE_KEY, Page.CONTENT_TYPE, "Foobar", new Date());
    }

    // CONFDEV-1788
    public void TODOXHTML_testSearchByLastModifiedDate()
    {
        String queryString = "Email archiving"; /* This query should return two content in the demo space, last modified sometime Aug 20, 2007 */

        createPage(TEST_SPACE_KEY, queryString, "");


        /* Search back just one day so that it returns only the page we created above */
        long testPageId = createPage(TEST_SPACE_KEY, "testSearchByLastModifiedDate",
                "{search:query=" + queryString + "|lastModified=1d}"
        );

        getIndexHelper().update();

        viewPageById(testPageId);

        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][1]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][2]");

        assertSearchResultContainsRow(TEST_SPACE_KEY,  Page.CONTENT_TYPE, queryString, new Date());
    }

    public void testSearchByContributor()
    {
        String otherUser = "otheruser";
        createUser(otherUser, otherUser, otherUser, new String[] { "confluence-users", "confluence-administrators" });

        logout();
        login(otherUser, otherUser);

        String pageTitle = "foobar";

        createPage(TEST_SPACE_KEY, pageTitle + " created by " + otherUser, StringUtils.EMPTY);
        logout();

        loginAsAdmin();
        createPage(TEST_SPACE_KEY, pageTitle + " created by " + getConfluenceWebTester().getAdminUserName(), StringUtils.EMPTY);

        long testPageId = createPage(TEST_SPACE_KEY, "testSearchByContributor", "{search:query=" + pageTitle + "|contributor=" + otherUser + "}");

        getIndexHelper().update();

        viewPageById(testPageId);



        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][1]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][2]");

        assertSearchResultContainsRow(TEST_SPACE_KEY,  Page.CONTENT_TYPE, pageTitle + " created by " + otherUser, new Date());
    }

    private void editTestSpaceDescription(String keyword)
    {
/* TODOXHTML CONFDEV-1224 renable when we make a decision on how to edit space descriptions in XHTML.
                          Ans: They are going to be plain text (with an XHTML body content type)
        gotoPage("/spaces/editspace.action?key=" + TEST_SPACE_KEY);
        setWorkingForm("editspace");
        setTextField("description", keyword);
        submit("confirm");
*/
    }

    private void editCurrentUserInfo(String info)
    {
        gotoPage("/users/editmyprofile.action");
        setWorkingForm("editmyprofileform");
        setTextField("personalInformation", info);
        submit("confirm");
    }

    public void testSearchByPageType() throws UnsupportedEncodingException
    {
        String keyword = "foobar";

        editTestSpaceDescription(keyword);
        editCurrentUserInfo(keyword);

        long pageWithKeywordId = createPage(TEST_SPACE_KEY, keyword, StringUtils.EMPTY);
        createComment(pageWithKeywordId, 0, keyword);
        createBlogPost(TEST_SPACE_KEY,  keyword, StringUtils.EMPTY);
        createAttachment(pageWithKeywordId, keyword + ".txt", keyword.getBytes("UTF-8"), "text/plain");

        getIndexHelper().update();

        long testPageId = createPage(
                TEST_SPACE_KEY, "testSearchByPageType",
                "{search:query=" + keyword + "|type=page}"
        );

        viewPageById(testPageId);


        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][1]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][2]");

        assertSearchResultContainsRow(TEST_SPACE_KEY,  Page.CONTENT_TYPE, keyword, new Date());
    }

    public void testSearchByBlogPostType() throws UnsupportedEncodingException
    {
        String keyword = "foobar";

        editTestSpaceDescription(keyword);
        editCurrentUserInfo(keyword);

        long pageWithKeywordId = createPage(TEST_SPACE_KEY, keyword, StringUtils.EMPTY);
        createComment(pageWithKeywordId, 0, keyword);
        createBlogPost(TEST_SPACE_KEY,  keyword, StringUtils.EMPTY);
        createAttachment(pageWithKeywordId, keyword + ".txt", keyword.getBytes("UTF-8"), "text/plain");

        getIndexHelper().update();

        long testPageId = createPage(
                TEST_SPACE_KEY, "testSearchByBlogPostType",
                "{search:query=" + keyword + "|type=blogpost}"
        );

        viewPageById(testPageId);


        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][1]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][2]");

        assertSearchResultContainsRow(TEST_SPACE_KEY,  BlogPost.CONTENT_TYPE, keyword, new Date());
    }

    public void testSearchByCommentType() throws UnsupportedEncodingException
    {
        String keyword = "foobar";

        editTestSpaceDescription(keyword);
        editCurrentUserInfo(keyword);

        long pageWithKeywordId = createPage(TEST_SPACE_KEY, keyword, StringUtils.EMPTY);
        createComment(pageWithKeywordId, 0, keyword);
        createBlogPost(TEST_SPACE_KEY,  keyword, StringUtils.EMPTY);
        createAttachment(pageWithKeywordId, keyword + ".txt", keyword.getBytes("UTF-8"), "text/plain");

        getIndexHelper().update();

        long testPageId = createPage(
                TEST_SPACE_KEY, "testSearchByCommentType",
                "{search:query=" + keyword + "|type=comment}"
        );

        viewPageById(testPageId);


        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][1]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][2]");

        assertSearchResultContainsRow(TEST_SPACE_KEY,  Comment.CONTENT_TYPE, "Re: " + keyword, new Date());
    }

    public void testSearchByAttachmentType() throws UnsupportedEncodingException
    {
        String keyword = "foobar";

        editTestSpaceDescription(keyword);
        editCurrentUserInfo(keyword);

        long pageWithKeywordId = createPage(TEST_SPACE_KEY, keyword, StringUtils.EMPTY);
        createComment(pageWithKeywordId, 0, keyword);
        createBlogPost(TEST_SPACE_KEY,  keyword, StringUtils.EMPTY);
        createAttachment(pageWithKeywordId, keyword + ".txt", keyword.getBytes("UTF-8"), "text/plain");

        getIndexHelper().update();

        long testPageId = createPage(
                TEST_SPACE_KEY, "testSearchByAttachmentType",
                "{search:query=" + keyword + "|type=attachment}"
        );

        viewPageById(testPageId);


        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][1]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][2]");

        assertSearchResultContainsRow(TEST_SPACE_KEY,  Attachment.CONTENT_TYPE, keyword + ".txt", new Date());
    }

    // TODOXHTML CONFDEV-1224
    public void TODOXHTML_testSearchBySpaceDescType() throws UnsupportedEncodingException
    {
        String keyword = "foobar";

        editTestSpaceDescription(keyword);
        editCurrentUserInfo(keyword);

        long pageWithKeywordId = createPage(TEST_SPACE_KEY, keyword, StringUtils.EMPTY);
        createComment(pageWithKeywordId, 0, keyword);
        createBlogPost(TEST_SPACE_KEY,  keyword, StringUtils.EMPTY);
        createAttachment(pageWithKeywordId, keyword + ".txt", keyword.getBytes("UTF-8"), "text/plain");

        getIndexHelper().update();

        long testPageId = createPage(
                TEST_SPACE_KEY, "testSearchBySpaceDescType",
                "{search:query=" + keyword + "|type=spacedesc}"
        );

        viewPageById(testPageId);


        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][1]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][2]");

        assertSearchResultContainsRow(TEST_SPACE_KEY,  SpaceDescription.CONTENT_TYPE_SPACEDESC, TEST_SPACE_TITLE, new Date());
    }

    public void testSearchByUserInfoType() throws UnsupportedEncodingException
    {
        String keyword = "foobar";

        editTestSpaceDescription(keyword);
        editCurrentUserInfo(keyword);

        long pageWithKeywordId = createPage(TEST_SPACE_KEY, keyword, StringUtils.EMPTY);
        createComment(pageWithKeywordId, 0, keyword);
        createBlogPost(TEST_SPACE_KEY,  keyword, StringUtils.EMPTY);
        createAttachment(pageWithKeywordId, keyword + ".txt", keyword.getBytes("UTF-8"), "text/plain");

        getIndexHelper().update();

        long testPageId = createPage(
                TEST_SPACE_KEY, "testSearchByUserInfoType",
                "{search:query=" + keyword + "|type=userinfo}"
        );

        viewPageById(testPageId);


        assertElementPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][1]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[@class='searchMacro']//div[@class='result'][2]");

        UserHelper userHelper = getUserHelper(getConfluenceWebTester().getCurrentUserName());

        assertTrue(userHelper.read());

        assertSearchResultContainsRow(TEST_SPACE_KEY,  PersonalInformation.CONTENT_TYPE, userHelper.getFullName(), new Date());
    }
}
