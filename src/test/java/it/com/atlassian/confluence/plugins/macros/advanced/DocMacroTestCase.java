package it.com.atlassian.confluence.plugins.macros.advanced;

public class DocMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    public void testLinkSuppliedIsRelativeToCac()
    {
        String linkText = "linkText";

        viewPageById(
                createPage(
                        TEST_SPACE_KEY, "testLinkSuppliedIsRelativeToCac", "{doc:display/CONFEXT}" + linkText + "{doc}"
                )
        );

        assertEquals(
                "http://confluence.atlassian.com/display/CONFEXT",
                getElementAttributeByXPath("//a[text()='" + linkText + "']", "href")
        );
    }

    public void testXSSVulnerability()
    {
    	String scriptText = "\"/><input><b<script>alert(1)</script>0<select>yyyyxxxxx}\"/><input>xxxxx";

    	viewPageById(
    			createPage(
    					TEST_SPACE_KEY, "testXSSVulnerability", "{doc:" + scriptText + "{doc}"
    			)
    	);

    	assertEquals(
    			"http://confluence.atlassian.com/" + scriptText.substring(0, 55),
                getElementAttributeByXPath("//div[@class='wiki-content']//a", "href"));
    }
}
