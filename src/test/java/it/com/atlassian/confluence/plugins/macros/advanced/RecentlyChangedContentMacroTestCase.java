package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;
import com.atlassian.xwork.interceptors.XsrfTokenInterceptor;
import junit.framework.AssertionFailedError;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static java.util.Arrays.asList;


public class RecentlyChangedContentMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private static final String MACRO_NAME = "recently-updated";
    private static final String BLANK_MESSAGE =
            "As you and your team create content this area will fill up and display the latest updates.";

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        deleteSpace("ds"); /* We don't like the default content to be there */
        getIndexHelper().update();
    }

    private void createTestSpace()
    {
        createSpace(TEST_SPACE_KEY, TEST_SPACE_TITLE, TEST_SPACE_DESCRIPTION);
    }

    public void testSubHeadingHasIdAttribute() throws Exception
    {
        final int pagesToCreate = 10;
        final String pageContent = "{" + MACRO_NAME + ":max=" + pagesToCreate + "}" + "\n" +
                                   "{" + MACRO_NAME + ":max=" + pagesToCreate + ",hideHeading=true}" + "\n" +
                                   "{" + MACRO_NAME + ":max=" + pagesToCreate + "}" + "\n" +
                                   "{" + MACRO_NAME + ":max=" + pagesToCreate + "}";

        final long testPageId = createPage(TEST_SPACE_KEY, "testSubHeadingHasIdAttribute", pageContent);

        for (int i = 0; i < pagesToCreate; ++i)
            createPage(TEST_SPACE_KEY, "Page " + i, "Page " + i);

        viewPageById(testPageId);
        assertElementPresent("recently-updated-macro");
        //the 2nd macro invocation creates an different (unique) heading
        assertElementPresent("recently-updated-macro.1");
        assertElementPresent("recently-updated-macro.2");
        assertElementNotPresent("recently-updated-macro.3");
    }

    public void testMaxParameterLimitsNumberOfItemsShown()
    {
        final int pagesToCreate = 10;
        final long testPageId = createPage(TEST_SPACE_KEY, "testMaxParameterLimitsNumberOfItemsShown", "{" + MACRO_NAME + ":max=" + pagesToCreate + "}");

        for (int i = 0; i < pagesToCreate; ++i)
            createPage(TEST_SPACE_KEY, "Page " + i, "Page " + i);

        getIndexHelper().update();

        viewPageById(testPageId);

        for (int i = pagesToCreate - 1, j = 1; i >= 0; --i, ++j)
            assertForTextInRow(j, "Page " + i);
    }

    /**
     * Asserts that text occurs in the specified order in the the i-th row.
     * @param i ith row (using 1-based index)
     */
    private void assertForTextInRow(int i, String... texts)
    {
        final String rowText = getElementTextByXPath("//div[@class='results-container']/ul/li[" + i + "]");

        int lastMatchIndex = -1;
        int matchIndex;
        for (String text : texts)
        {
            matchIndex = rowText.indexOf(text);
            assertTrue("Could not find text: '" + text + "' in '" + rowText + "'", matchIndex != -1);
            assertTrue(text + " found but out of order", matchIndex > lastMatchIndex);
            lastMatchIndex = matchIndex;
        }
    }

    private void assertForProfileImage()
    {
        assertElementPresentByXPath("//div[@class='results-container']//img[@class='userLogo logo']");
    }

    private void assertNumberOfResults(int expectedResults)
    {
        try
        {
            assertElementNotPresentByXPath("//div[@class='results-container']/ul/li[" + (expectedResults + 1) + "]");
        }
        catch (AssertionFailedError e)
        {
            throw new AssertionFailedError("Expected only " + expectedResults + " results. Found more.");
        }
    }

    public void testMaxResultsParameterLimitsNumberOfItemsShown()
    {
        final int pagesToCreate = 10;
        final long testPageId = createPage(TEST_SPACE_KEY, "testMaxResultsParameterLimitsNumberOfItemsShown", "{" + MACRO_NAME + ":maxResults=" + pagesToCreate + "}");

        for (int i = 0; i < pagesToCreate; ++i)
            createPage(TEST_SPACE_KEY, "Page " + i, "Page " + i);

        getIndexHelper().update();

        viewPageById(testPageId);

        for (int i = pagesToCreate - 1, j = 1; i >= 0; --i, ++j)
            assertForTextInRow(j, "Page " + i);
    }

    public void testMultipleUpdatesForSinglePageAreDisplayed()
    {
        final String testUser1 = "testuser1";
        final String testUser2 = "testuser2";

        createUser(testUser1, testUser1, testUser1, new String[] { "confluence-administrators" });
        createUser(testUser2, testUser2, testUser2, new String[] { "confluence-administrators" });
        logout();

        login(testUser1, testUser1);
        final long testPageId = createPage(TEST_SPACE_KEY, TEST_PAGE_TITLE, "", asList("foo"));
        updatePage(testPageId, "foo");
        logout();

        login(testUser2, testUser2);
        updatePage(testPageId, "bar");
        updatePage(testPageId, "baz");
        logout();

        login(testUser1, testUser1);
        updatePage(testPageId, "{" + MACRO_NAME + ":labels=foo}");
        getIndexHelper().update();
        viewPageById(testPageId);
        assertLinkPresentWithExactText(TEST_PAGE_TITLE, 0);
        assertLinkPresentWithExactText(TEST_PAGE_TITLE, 1);
        assertTextsPresentInOrder(new String[]{"by", ">" + testUser1 + "<", "by", ">" + testUser2 + "<"});
        logout();
    }

    public void testContentFilterByMultipleAuthors()
    {
        final String userOne = "jdoe";
        final String userTwo = "jsmith";
        final String doePage = "Page by Doe";
        final String smithPage = "Page by Smith";

        createUser(userOne, userOne, userOne, new String[] { "confluence-administrators" });
        createUser(userTwo, userTwo, userTwo, new String[] { "confluence-administrators" });
        logout();

        login(userOne, userOne);
        createPage(TEST_SPACE_KEY, doePage, StringUtils.EMPTY);
        logout();

        login(userTwo, userTwo);
        createPage(TEST_SPACE_KEY, smithPage, StringUtils.EMPTY);
        logout();

        loginAsAdmin();
        final long testPageId = createTestPage();
        updatePage(testPageId, "{" + MACRO_NAME + "}");
        getIndexHelper().update();
        viewPageById(testPageId);
        assertLinkPresentWithExactText(doePage);
        assertLinkPresentWithExactText(smithPage);

        updatePage(testPageId, "{" + MACRO_NAME + ":author=" + userOne + "}");
        getIndexHelper().update();
        viewPageById(testPageId);
        assertLinkPresentWithExactText(doePage);
        assertLinkNotPresentWithExactText(smithPage);

        updatePage(testPageId, "{" + MACRO_NAME + ":author=" + userTwo + "}");
        getIndexHelper().update();
        viewPageById(testPageId);
        assertLinkNotPresentWithExactText(doePage);
        assertLinkPresentWithExactText(smithPage);
        logout();
    }


    public void testContentFilterByLabelExclusion()
    {
        final String pageWithExcludedLabelTitle = "pageWithExcludedLabelTitle";
        final String pageWithoutExcludedLabelTitle = "pageWithoutExcludedLabelTitle";
        final long pageWithExcludedLabel = createPage(TEST_SPACE_KEY, pageWithExcludedLabelTitle, StringUtils.EMPTY);
        final long pageWithoutExcludedLabel = createPage(TEST_SPACE_KEY, pageWithoutExcludedLabelTitle, StringUtils.EMPTY);
        final String excludedLabel = "foo";
        final String label = "bar";

        addLabelToPage(excludedLabel, pageWithExcludedLabel);
        addLabelToPage(label, pageWithoutExcludedLabel);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByLabelExclusion",
                "{" + MACRO_NAME + ":labels=-" + excludedLabel + "," + label + "}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertTextNotPresent(pageWithExcludedLabelTitle);
        assertForTextInRow(1, pageWithoutExcludedLabelTitle);
        assertNumberOfResults(1);
    }

    public void testContentFilterByImplicitLabels()
    {
        final String pageWithLabelTitle = "pageWithLabelTitle";
        final String anotherPageWithLabelTitle = "anotherPageWithLabelTitle";
        final long pageWithLabel1 = createPage(TEST_SPACE_KEY, pageWithLabelTitle, StringUtils.EMPTY);
        final long pageWithLabel2 = createPage(TEST_SPACE_KEY, anotherPageWithLabelTitle, StringUtils.EMPTY);
        final String[] labels = new String[] { "label1", "label2" };

        addLabelToPage(labels[0], pageWithLabel1);
        addLabelToPage(labels[1], pageWithLabel2);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByImplicitLabels",
                "{" + MACRO_NAME + ":labels=" + labels[0] + "," + labels[1] + "}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, anotherPageWithLabelTitle);
        assertForTextInRow(2, pageWithLabelTitle);
        assertNumberOfResults(2);
    }

    public void testContentFilterByExplicitLabels()
    {
        final String pageWithAllLabelsTitle = "pageWithAllLabelsTitle";
        final String pageWithoutAllLabelsTitle = "pageWithoutAllLabelsTitle";
        final long pageWithExplicitLabels = createPage(TEST_SPACE_KEY, pageWithAllLabelsTitle, StringUtils.EMPTY);
        final long pageWithoutExplicitLabels = createPage(TEST_SPACE_KEY, pageWithoutAllLabelsTitle, StringUtils.EMPTY);
        final String[] explicitLabels = new String[] { "label1", "label2" };

        addLabelToPage(explicitLabels[0], pageWithExplicitLabels);
        addLabelToPage(explicitLabels[1], pageWithExplicitLabels);

        addLabelToPage(explicitLabels[0], pageWithoutExplicitLabels);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByExplicitLabels",
                "{" + MACRO_NAME + ":labels=+" + explicitLabels[0] + ",+" + explicitLabels[1] + "}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageWithAllLabelsTitle);
        assertNumberOfResults(1);
    }

    public void testContentFilteredByLegacyLabelParameter()
    {
        final String pageWithLabelTitle = "pageWithLabelTitle";
        final String pageWithoutLabelTitle = "pageWithoutLabelTitle";
        final long pageWithLabel1 = createPage(TEST_SPACE_KEY, pageWithLabelTitle, StringUtils.EMPTY);

        createPage(TEST_SPACE_KEY, pageWithoutLabelTitle, StringUtils.EMPTY);
        addLabelToPage("label1", pageWithLabel1);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByLegacyLabelParameter",
                "{" + MACRO_NAME + ":labels=label1}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageWithLabelTitle);
        assertNumberOfResults(1);
    }

    public void testContentFilteredByAnyOfLabelsSpecifiedAsLegacyLabelParameter()
    {
        final String pageWithLabelTitle1 = "pageWithLabelTitle1";
        final String pageWithLabelTitle2 = "pageWithLabelTitle2";
        final long pageWithLabel1 = createPage(TEST_SPACE_KEY, pageWithLabelTitle1, StringUtils.EMPTY);
        final long pageWithLabel2 = createPage(TEST_SPACE_KEY, pageWithLabelTitle2, StringUtils.EMPTY);

        addLabelToPage("label1", pageWithLabel1);
        addLabelToPage("label2", pageWithLabel2);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByAnyOfLabelsSpecifiedAsLegacyLabelParameter",
                "{" + MACRO_NAME + ":labels=label1,label2}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageWithLabelTitle2);
        assertForTextInRow(2, pageWithLabelTitle1);
        assertNumberOfResults(2);
    }

    // ADVMACROS-65 CONF-10167
    // Make sure non-existent labels appropriately limit the results
    public void testContentFilteredByNonexistentLabels()
    {
        final String pageWithLabelTitle1 = "pageWithLabelTitle1";
        final String pageWithLabelTitle2 = "pageWithLabelTitle2";
        final long pageWithLabel1 = createPage(TEST_SPACE_KEY, pageWithLabelTitle1, StringUtils.EMPTY);
        final long pageWithLabel2 = createPage(TEST_SPACE_KEY, pageWithLabelTitle2, StringUtils.EMPTY);

        addLabelToPage("label1", pageWithLabel1);
        addLabelToPage("label1", pageWithLabel2);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByNonexistentLabels",
                "{" + MACRO_NAME + ":labels=label2}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);
        assertTextPresent(BLANK_MESSAGE);
    }

    public void testSortReverseWithoutSortParamHasNoEffect()
    {
        final String olderPageTitle = "olderPageTitle";
        final String newerPageTitle = "newerPageTitle";
        final String labelToLimitResults = "labelone";

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testSortReverseWithoutSortParamDoesNotSortContentIsAscendingModifiedDate",
                "{" + MACRO_NAME + ":reverse=true|labels=" + labelToLimitResults + "}");

        createPage(TEST_SPACE_KEY, olderPageTitle, StringUtils.EMPTY, Arrays.asList(labelToLimitResults));
        createPage(TEST_SPACE_KEY, newerPageTitle, StringUtils.EMPTY, Arrays.asList(labelToLimitResults));

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, newerPageTitle);
        assertForTextInRow(2, olderPageTitle);
    }

    public void testSortByTitle()
    {
        final String firstPageTitle = "a";
        final String secondPageTitle = "b";
        final String labelToLimitResult = "labelone";

        createPage(TEST_SPACE_KEY, firstPageTitle, StringUtils.EMPTY, Arrays.asList(labelToLimitResult));
        createPage(TEST_SPACE_KEY, secondPageTitle, StringUtils.EMPTY, Arrays.asList(labelToLimitResult));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testSortByTitle",
                "{" + MACRO_NAME + ":sort=title|labels=" + labelToLimitResult + "}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, firstPageTitle);
        assertForTextInRow(2, secondPageTitle);
        assertNumberOfResults(2);
    }

    public void testSortByTitleReversed()
    {
        final String firstPageTitle = "a";
        final String secondPageTitle = "b";
        final String labelToLimitResult = "labelone";

        createPage(TEST_SPACE_KEY, firstPageTitle, StringUtils.EMPTY, Arrays.asList(labelToLimitResult));
        createPage(TEST_SPACE_KEY, secondPageTitle, StringUtils.EMPTY, Arrays.asList(labelToLimitResult));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testSortByTitleReversed",
                "{" + MACRO_NAME + ":sort=title|reverse=true|labels=" + labelToLimitResult + "}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, firstPageTitle);
        assertForTextInRow(2, secondPageTitle);
        assertNumberOfResults(2);
    }

    public void testSortCreationDate()
    {
        final String firstPageTitle = "firstPageTitle";
        final String secondPageTitle = "secondPageTitle";
        final long firstPageId = createPage(TEST_SPACE_KEY, firstPageTitle, StringUtils.EMPTY);
        final long secondPageId = createPage(TEST_SPACE_KEY, secondPageTitle, StringUtils.EMPTY);

        addLabelToPage("tolimitresults", firstPageId);
        addLabelToPage("tolimitresults", secondPageId);

        /* We update the second page first so that we are sure the order is not by modified date */
        updatePage(secondPageId);
        updatePage(firstPageId);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testSortCreationDate",
                "{" + MACRO_NAME + ":sort=creation|labels=tolimitresults}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, firstPageTitle);
        assertForTextInRow(2, secondPageTitle);
        assertNumberOfResults(2);
    }

    public void testFilterContentBySpacesSelf() throws IOException
    {
        final File restoreDataTempFile = restoreDataFromZipClassPathResource("content-filtering-space-param-test-data.zip");

        try
        {
            final String otherSpaceKey = "os";
            final String otherSpacePageTitle = "otherSpacePageTitle";

            createSpace(otherSpaceKey, otherSpaceKey, StringUtils.EMPTY);
            createPage(otherSpaceKey, otherSpacePageTitle, StringUtils.EMPTY);

            createTestSpace();

            final long macroContainerPageId = createPage(
                    TEST_SPACE_KEY,
                    "testFilterContentBySpacesSelf",
                    "{" + MACRO_NAME + ":spaces=@self}");
            getIndexHelper().update();

            viewPageById(macroContainerPageId);

            assertElementNotPresentByXPath("//table[@class='tableview']//tr/td/a[@title='" + otherSpacePageTitle + "']");
        }
        finally
        {
            restoreDataTempFile.delete();
        }
    }

    public void testFilterContentBySpacesPersonal() throws IOException
    {
        final File restoreDataTempFile = restoreDataFromZipClassPathResource("content-filtering-space-param-test-data.zip");

        try
        {
            final String personalSpacePageTitle = "personalSpacePageTitle";

            createPage("~" + getConfluenceWebTester().getCurrentUserName(), personalSpacePageTitle, StringUtils.EMPTY);
            createTestSpace();

            final long macroContainerPageId = createPage(
                    TEST_SPACE_KEY,
                    "testFilterContentBySpacesPersonal",
                    "{" + MACRO_NAME + ":spaces=@personal}");
            getIndexHelper().update();

            viewPageById(macroContainerPageId);

            assertForTextInRow(1, personalSpacePageTitle);
        }
        finally
        {
            restoreDataTempFile.delete();
        }
    }

    public void testFilterContentBySpacesGlobal() throws IOException
    {
        final File restoreDataTempFile = restoreDataFromZipClassPathResource("content-filtering-space-param-test-data.zip");

        try
        {
            final String pageInFavGlobalSpaceTitle = "pageInFavGlobalSpaceTitle";
            final String pageInNonFavGlobalSpaceTitle = "pageInNonFavGlobalSpaceTitle";
            final String pageInPersonalSpaceTitle = "pageInPersonalSpaceTitle";

            createPage("ugs", pageInNonFavGlobalSpaceTitle, StringUtils.EMPTY);
            createPage("fgs", pageInFavGlobalSpaceTitle, StringUtils.EMPTY);
            createPage("~" + getConfluenceWebTester().getCurrentUserName(), pageInPersonalSpaceTitle, StringUtils.EMPTY);
            createTestSpace();

            final long macroContainerPageId = createPage(
                    TEST_SPACE_KEY,
                    "testFilterContentBySpacesGlobal",
                    "{" + MACRO_NAME + ":spaces=@global}");
            getIndexHelper().update();

            viewPageById(macroContainerPageId);

            assertForTextInRow(4, pageInFavGlobalSpaceTitle);
            assertForTextInRow(5, pageInNonFavGlobalSpaceTitle);
            assertTextNotPresent(pageInPersonalSpaceTitle);
        }
        finally
        {
            restoreDataTempFile.delete();
        }
    }

    public void testFilterContentBySpacesFavorite() throws IOException
    {
        final File restoreDataTempFile = restoreDataFromZipClassPathResource("content-filtering-space-param-test-data.zip");

        try
        {
            final String pageInFavGlobalSpaceTitle = "pageInFavGlobalSpaceTitle";
            final String pageInNonFavGlobalSpaceTitle = "pageInNonFavGlobalSpaceTitle";

            createPage("ugs", pageInNonFavGlobalSpaceTitle, StringUtils.EMPTY);
            createPage("fgs", pageInFavGlobalSpaceTitle, StringUtils.EMPTY);
            createTestSpace();

            final long macroContainerPageId = createPage(
                    TEST_SPACE_KEY,
                    "testFilterContentBySpacesFavorite",
                    "{" + MACRO_NAME + ":spaces=@favorite}");
            getIndexHelper().update();

            viewPageById(macroContainerPageId);

            assertForTextInRow(1, pageInFavGlobalSpaceTitle);
            assertTextNotPresent(pageInNonFavGlobalSpaceTitle);
        }
        finally
        {
            restoreDataTempFile.delete();
        }
    }

    public void testFilterContentBySpacesAll() throws IOException
    {
        final File restoreDataTempFile = restoreDataFromZipClassPathResource("content-filtering-space-param-test-data.zip");

        try
        {
            final String pageInFavGlobalSpaceTitle = "pageInFavGlobalSpaceTitle";
            final String pageInNonFavGlobalSpaceTitle = "pageInNonFavGlobalSpaceTitle";
            final String pageInPersonalSpaceTitle = "pageInPersonalSpaceTitle";
            final String testPageTitle = "testFilterContentBySpacesAll";

            createTestSpace();

            createPage("~" + getConfluenceWebTester().getCurrentUserName(), pageInPersonalSpaceTitle, StringUtils.EMPTY);
            createPage("ugs", pageInNonFavGlobalSpaceTitle, StringUtils.EMPTY);
            createPage("fgs", pageInFavGlobalSpaceTitle, StringUtils.EMPTY);

            final long macroContainerPageId = createPage(
                    TEST_SPACE_KEY,
                    testPageTitle,
                    "{" + MACRO_NAME + ":spaces=@all}");
            getIndexHelper().update();

            viewPageById(macroContainerPageId);

            assertForTextInRow(1, testPageTitle);
            assertForTextInRow(2, pageInFavGlobalSpaceTitle);
            assertForTextInRow(3, pageInNonFavGlobalSpaceTitle);
            assertForTextInRow(4, pageInPersonalSpaceTitle);
        }
        finally
        {
            restoreDataTempFile.delete();
        }
    }

    public void testFilterContentByPage()
    {

        final String pageTitle = "pageTitle";
        final String blogTitle = "first post";

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testFilterContentByPage",
                "{" + MACRO_NAME + ":types=page}");
        createPage(TEST_SPACE_KEY, pageTitle, pageTitle);
        createBlogPost(TEST_SPACE_KEY, blogTitle, "");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageTitle);
        assertLinkNotPresentWithExactText(blogTitle);
    }

    public void testFilterContentByBlogPost()
    {
        final String pageTitle = "pageTitle";
        final String blogTitle = "first post";

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testFilterContentByPage",
                "{" + MACRO_NAME + ":types=blogpost}");
        createPage(TEST_SPACE_KEY, pageTitle, pageTitle);
        createBlogPost(TEST_SPACE_KEY, blogTitle, "");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, blogTitle);
        assertNumberOfResults(1);
        assertLinkNotPresentWithExactText(pageTitle);
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-60">ADVMACROS-60</a>
     */
    public void testFilterContentByNews()
    {
        final String blogPostTitle = "blogPostTitle";
        createBlogPost(TEST_SPACE_KEY, blogPostTitle, blogPostTitle);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testFilterContentByNews",
                "{" + MACRO_NAME + ":types=news}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, blogPostTitle);
        assertNumberOfResults(1);
    }

    public void testFilterContentByComment()
    {
        final String pageTitle = "pageTitle";
        createComment(
                createPage(TEST_SPACE_KEY, pageTitle, pageTitle),
                0,
                pageTitle);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testFilterContentByComment",
                "{" + MACRO_NAME + ":types=comment}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageTitle, "commented by");
        assertNumberOfResults(1);
    }

    public void testFilterContentByAttachment()
    {
        final String pageTitle = "pageTitle";
        final String attachmentFileName = "attachmentFileName";

        createAttachment(
                createPage(TEST_SPACE_KEY, pageTitle, pageTitle),
                attachmentFileName,
                new byte[1],
                null);

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testFilterContentByAttachment",
                "{" + MACRO_NAME + ":types=attachment}");
        getIndexHelper().update();

        viewPageById(macroContainerPageId);
    }

    public void testWidthNotIgnored()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testWidthNotIgnored");
        pageHelper.setContent("{" + MACRO_NAME + ":width=50%|theme=" + Theme.concise.name() + "}");
        assertTrue(pageHelper.create());
        getIndexHelper().update();

        viewPageById(pageHelper.getId());

            assertTrue(getElementAttributeByXPath("//div[@class='recently-updated recently-updated-concise']", "style").contains("width: 50%"));
    }

    /**
     * <a href="http://jira.atlassian.com/browse/CONF-13504">CONF-13504</a>
     */
    public void testShowProfilePicSupported()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testShowProfilePicSupported");
        pageHelper.setContent("{" + MACRO_NAME + ":showProfilePic=true}");

        assertTrue(pageHelper.create());
        getIndexHelper().update();

        viewPageById(pageHelper.getId());

        assertForProfileImage();
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-65">ADVMACROS-65</a>
     */
//    public void testNoContentShownIfNoPageMatchesLabel()
//    {
//        final PageHelper pageHelper = getPageHelper();
//
//        pageHelper.setSpaceKey(TEST_SPACE_KEY);
//        pageHelper.setTitle("testNoContentShownIfNoPageMatchesLabel");
//        pageHelper.setContent("{" + MACRO_NAME + ":labels=nonexistent}");
//
//        assertTrue(pageHelper.create());
//        getIndexHelper().update();
//
//        viewPageById(pageHelper.getId());
//
//        assertEquals("recently-updated: 'nonexistent' is not an existing label", getElementTextByXPath("//div[@class='wiki-content']/div[@class='error']"));
//    }

    /**
     * <a href="http://jira.atlassian.com/browse/CONF-13502">CONF-13502</a>
     */
// Reinstate when we put space names back into the concise theme --dloeng
//    public void testSpaceNameShownWhenMultipleSpacesIsSpecified()
//    {
//        final PageHelper pageHelper = getPageHelper();
//
//        pageHelper.setSpaceKey(TEST_SPACE_KEY);
//        pageHelper.setTitle("testSpaceNameShownWhenMultipleSpacesIsSpecified");
//        pageHelper.setContent("{" + MACRO_NAME + ":spaces=*|width=50%|labels=confluence,jira|types=page}");
//
//        assertTrue(pageHelper.create());
//
//        addLabelToPage("confluence", pageHelper.getId());
//        addLabelToPage("jira", pageHelper.getId());
//
//        getIndexHelper().update();
//
//        viewPageById(pageHelper.getId());
//
//        assertForTextInRow(TEST_SPACE_TITLE, 1);
//    }

    /**
     * <a href="http://jira.atlassian.com/browse/CONF-13751">CONF-13751</a>
     */
    public void testDefaultMaxSizeRespected()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testDefaultMaxSizeRespected");
        pageHelper.setContent("{" + MACRO_NAME + "}"); /* No max size specified */

        assertTrue(pageHelper.create());

        /* The default is 15, create more than that */
        for (int i = 0; i < 16; ++i)
            createPage(TEST_SPACE_KEY,  "Page " + i, StringUtils.EMPTY);

        getIndexHelper().update();

        viewPageById(pageHelper.getId());
        assertElementNotPresentByXPath("//table[@class='tableview']//tr[16]");
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-67">ADVMACROS-67</a>
     */
    public void testAttachmentNamesWithHtmlUnsafeCharactersProperlyEscaped()
    {
        final String atachmentFileName = "<script>\"alert('hello')\"</script>";
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testAttachmentNamesWithHtmlUnsafeCharactersProperlyEscaped");
        pageHelper.setContent("{" + MACRO_NAME + "}"); /* No max size specified */

        assertTrue(pageHelper.create());

        createAttachment(pageHelper.getId(), atachmentFileName, new byte[1], null);

        getIndexHelper().update();

        viewPageById(pageHelper.getId());

        assertForTextInRow(1, atachmentFileName);
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-74">ADVMACROS-74</a>
     */
    public void testShowMoreButtonNotGeneratedByTemplateWithProfilePic()
    {
        /* Create lots of pages */
        for (int i = 0; i < 16; ++i)
            createPage(TEST_SPACE_KEY,  "Page " + i, StringUtils.EMPTY);


        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testShowMoreButtonNotGeneratedByTemplateWithProfilePic");
        pageHelper.setContent("{" + MACRO_NAME + ":showProfilePic=true}");

        assertTrue(pageHelper.create());

        getIndexHelper().update();

        viewPageById(pageHelper.getId());
        assertElementNotPresentByXPath("//a/img[@title='Show More']");
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-74">ADVMACROS-74</a>
     */
    public void testShowLessButtonNotGeneratedByTemplateWithProfilePic()
    {
        /* Create lots of pages */
        for (int i = 0; i < 16; ++i)
            createPage(TEST_SPACE_KEY,  "Page " + i, StringUtils.EMPTY);


        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testShowLessButtonNotGeneratedByTemplateWithProfilePic");
        pageHelper.setContent("{" + MACRO_NAME + ":showProfilePic=true}");

        assertTrue(pageHelper.create());

        getIndexHelper().update();

        /* Force the show less button to be there */
        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId() + "&maxRecentlyUpdatedPageCount=20");

        assertElementNotPresentByXPath("//a/img[@title='Show Fewer']");
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-74">ADVMACROS-74</a>
     */
    public void testShowMoreButtonNotGeneratedByTemplateWithoutProfilePic()
    {
        /* Create lots of pages */
        for (int i = 0; i < 16; ++i)
            createPage(TEST_SPACE_KEY,  "Page " + i, StringUtils.EMPTY);


        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testShowMoreButtonNotGeneratedByTemplateWithoutProfilePic");
        pageHelper.setContent("{" + MACRO_NAME + "}");

        assertTrue(pageHelper.create());

        getIndexHelper().update();

        viewPageById(pageHelper.getId());
        assertElementNotPresentByXPath("//a/img[@title='Show More']");
    }

    /**
     * <a href="http://developer.atlassian.com/jira/browse/ADVMACROS-74">ADVMACROS-74</a>
     */
    public void testShowLessButtonNotGeneratedByTemplateWithoutProfilePic()
    {
        /* Create lots of pages */
        for (int i = 0; i < 16; ++i)
            createPage(TEST_SPACE_KEY,  "Page " + i, StringUtils.EMPTY);


        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testShowLessButtonNotGeneratedByTemplateWithoutProfilePic");
        pageHelper.setContent("{" + MACRO_NAME + "}");

        assertTrue(pageHelper.create());

        getIndexHelper().update();

        /* Force the show less button to be there */
        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId() + "&maxRecentlyUpdatedPageCount=20");

        assertElementNotPresentByXPath("//a/img[@title='Show Fewer']");
    }

    public void testInvalidSpaceKeysReported()
    {
        final String invalidSpaceKey = "foobar";
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(TEST_SPACE_KEY);
        pageHelper.setTitle("testInvalidSpaceKeysReported");
        pageHelper.setContent("{" + MACRO_NAME + ":spaces=" + invalidSpaceKey +"}");

        assertTrue(pageHelper.create());

        getIndexHelper().update();

        viewPageById(pageHelper.getId());
        assertTextPresent(BLANK_MESSAGE);
    }

    public void testContentFilterByPersonalLabel()
    {
        final String pageWithPersonalLabel = "pageWithPersonalLabel";
        final String pageWithGlobalLabel = "pageWithGlobalLabel";
        final String personalLabel = "my:label";
        final String globalLabel= "label";

        createPage(TEST_SPACE_KEY, pageWithPersonalLabel, StringUtils.EMPTY, Arrays.asList(personalLabel));
        createPage(TEST_SPACE_KEY, pageWithGlobalLabel, StringUtils.EMPTY, Arrays.asList(globalLabel));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByPersonalLabel",
                "{" + MACRO_NAME + ":labels=" + personalLabel + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageWithPersonalLabel);
        assertNumberOfResults(1);
    }

    public void testContentFilterByGlobalLabel()
    {
        final String pageWithPersonalLabel = "pageWithPersonalLabel";
        final String pageWithGlobalLabel = "pageWithGlobalLabel";
        final String personalLabel = "my:label";
        final String globalLabel= "label";

        createPage(TEST_SPACE_KEY, pageWithPersonalLabel, StringUtils.EMPTY, Arrays.asList(personalLabel));
        createPage(TEST_SPACE_KEY, pageWithGlobalLabel, StringUtils.EMPTY, Arrays.asList(globalLabel));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilteredByGlobalLabel",
                "{" + MACRO_NAME + ":labels=" + globalLabel + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageWithGlobalLabel);
        assertNumberOfResults(1);
    }

    public void testContentFilterByFavouriteLabel()
    {
        final String favoritePage = "favoritePage";
        final String pageWithGlobalLabel = "pageWithGlobalLabel";
        final String favoriteLabel = "my:favourite"; /* Only works with my:favourite */
        final String globalLabel= "favourite";

        final long favoritePageId = createPage(TEST_SPACE_KEY, favoritePage, StringUtils.EMPTY);
        final PageHelper favoritePageHelper = getPageHelper(favoritePageId);

        assertTrue(favoritePageHelper.read());
        favoritePageHelper.markFavourite();
        assertTrue(favoritePageHelper.isFavorite());

        createPage(TEST_SPACE_KEY, pageWithGlobalLabel, StringUtils.EMPTY, Arrays.asList(globalLabel));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByFavouriteLabel",
                "{" + MACRO_NAME + ":labels=" + favoriteLabel + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, favoritePage);
        assertNumberOfResults(1);
    }

    public void testContentFilterByMultipleOptionalPersonalLabels()
    {
        final String pageWithPersonalLabel1 = "pageWithPersonalLabel1";
        final String pageWithPersonalLabel2 = "pageWithPersonalLabel2";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        createPage(TEST_SPACE_KEY, pageWithPersonalLabel1, StringUtils.EMPTY, Arrays.asList(personalLabel1));
        createPage(TEST_SPACE_KEY, pageWithPersonalLabel2, StringUtils.EMPTY, Arrays.asList(personalLabel2));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByMultipleOptionalPersonalLabels",
                "{" + MACRO_NAME + ":labels=" + personalLabel1 + "," + personalLabel2 + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageWithPersonalLabel2);
        assertForTextInRow(2, pageWithPersonalLabel1);
        assertNumberOfResults(2);
    }

    public void testContentFilterByMultipleCompulsoryPersonalLabels()
    {
        final String pageWithAllPersonalLabels = "pageWithAllPersonalLabels";
        final String pageWithoutAllPersonalLabels = "pageWithoutAllPersonalLabels";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        createPage(TEST_SPACE_KEY, pageWithAllPersonalLabels, StringUtils.EMPTY, Arrays.asList(personalLabel1, personalLabel2));
        createPage(TEST_SPACE_KEY, pageWithoutAllPersonalLabels, StringUtils.EMPTY, Arrays.asList(personalLabel2));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByMultipleCompulsoryPersonalLabels",
                "{" + MACRO_NAME + ":labels=+" + personalLabel1 + ",+" + personalLabel2 + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageWithAllPersonalLabels);
        assertNumberOfResults(1);
    }

    public void testContentFilterByExcludingPersonalLabels()
    {
        final String pageWithoutExcludedLabels = "pageWithoutExcludedLabels";
        final String pageWithExcludedLabels = "pageWithExcludedLabels";
        final String personalLabel1 = "my:label1";
        final String personalLabel2 = "my:label2";

        createPage(TEST_SPACE_KEY, pageWithoutExcludedLabels, StringUtils.EMPTY, Arrays.asList(personalLabel1));
        createPage(TEST_SPACE_KEY, pageWithExcludedLabels, StringUtils.EMPTY, Arrays.asList(personalLabel1, personalLabel2));

        final long macroContainerPageId = createPage(
                TEST_SPACE_KEY,
                "testContentFilterByExcludingPersonalLabels",
                "{" + MACRO_NAME + ":labels=" + personalLabel1 + ",-" + personalLabel2 + "}");

        getIndexHelper().update();

        viewPageById(macroContainerPageId);

        assertForTextInRow(1, pageWithoutExcludedLabels);
        assertNumberOfResults(1);
    }

    public void testPaginationOnConciseTheme()
    {
        testPagination(Theme.concise);
    }

    public void testPaginationOnSocialTheme()
    {
        testPagination(Theme.social);
    }

    public void testPaginationOnSidebarTheme()
    {
        testPagination(Theme.sidebar);
    }

    private void testPagination(Theme theme)
    {
        final String title = "Test Page";

        final int numPages = 4;
        long[] pageIds = new long[numPages];
        for (int i = 1; i <= numPages; i++)
            pageIds[i - 1] = createPage(TEST_SPACE_KEY, title + i, "Test Content");
        getIndexHelper().update();

        String baseUrl = "/plugins/recently-updated/changes.action?spaceKeys=" + TEST_SPACE_KEY + "&contentTypes=page&theme=" + theme.name();

        // check second page of results
        gotoPage(baseUrl + "&pageSize=2&startIndex=2");
        assertLinkPresentWithExactText(title + "1");
        assertLinkPresentWithExactText(title + "2");
        assertLinkNotPresentWithExactText(title + "3");
        assertLinkNotPresentWithExactText(title + "4");

        // check first page of results
        gotoPage(baseUrl + "&pageSize=2&startIndex=0");
        assertLinkNotPresentWithExactText(title + "1");
        assertLinkNotPresentWithExactText(title + "2");
        assertLinkPresentWithExactText(title + "3");
        assertLinkPresentWithExactText(title + "4");
        assertLinkPresentWithText("Show More");
    }

    public void testPaginationOnNonExistantOrExpiredSearchToken()
    {
        String base = "/plugins/recently-updated/changes.action?spaceKeys=" + TEST_SPACE_KEY + "&theme=" + Theme.social.name() + "&startIndex=10";
        gotoPage(base + "&searchToken=123"); // try a search token that is not actually associated with any current searchers

        assertElementPresentByXPath("//li[contains(@class, 'update-item-error')]");
    }

    public void testXssInSocialTheme() throws JSONException
    {
        XmlRpcHelper rpc = XmlRpcHelper.getInstance(getConfluenceWebTester());
        String evilUsername = "\">some text";
        String evilFullname = "Script Kiddy";
        String evilPassword = "test";
        createUser(evilUsername, evilPassword, evilFullname);
        rpc.grantSpacePermission(XmlRpcHelper.VIEWSPACE_PERMISSION, evilUsername, TEST_SPACE_KEY);
        rpc.grantSpacePermission(XmlRpcHelper.CREATEEDIT_PAGE_PERMISSION, evilUsername, TEST_SPACE_KEY);
        rpc.grantGlobalPermission(XmlRpcHelper.UPDATE_USER_STATUS_PERMISSION, evilUsername);

        logout();
        login(evilUsername, evilPassword);

        createPage(TEST_SPACE_KEY, "evil page", "sometext");
        clickLink("create-personal-space-link");
        submit();

        setUserStatus("a new status");

        getIndexHelper().update();

        logout();
        loginAsAdmin();
        createPage(TEST_SPACE_KEY, "test", "{recently-updated:theme=social|spaces=@all}");
        gotoPage("display/" + TEST_SPACE_KEY + "/test");
        assertTextPresent("evil page");
        assertTextNotPresent("some text");
        //for some reason, getPageText() doesn't include the text inside the user anchor. Using pageSource to work around it.
        assertTrue(getPageSource().contains(evilFullname));
    }

    // http://developer.atlassian.com/jira/browse/ADVMACROS-115
    public void testContentByLabelMacroOutputShowsSpaceName()
    {
        long recentlyUpdatedPageWithoutProfilePicsId = createPage(
                TEST_SPACE_KEY, "Recently Updated", "{recently-updated:showProfilePic=false}",
                Arrays.asList("label1")
        );
        viewPageById(recentlyUpdatedPageWithoutProfilePicsId);

        getIndexHelper().update();

        long contentByLabelPage = createPage(
                TEST_SPACE_KEY, "testContentByLabelMacroOutputShowsSpaceName", "{contentbylabel:labels=label1|showSpace=true}"
        );
        viewPageById(contentByLabelPage);

        assertTextPresent("(" + TEST_SPACE_TITLE + ")");
    }

    public void testUserStatusWithLinkWithApostrophe() throws JSONException
    {
        String title = "Brian's page";
        createPage(TEST_SPACE_KEY, title, "some content");
        String linkText = "This is a status!";
        setUserStatus("[" + linkText + "|" + TEST_SPACE_KEY + ":" + title + "]");

        gotoPage("/users/viewmyprofile.action");
        assertLinkPresentWithExactText(linkText, 0);
        assertLinkPresentWithExactText(linkText, 1);
        clickLinkWithExactText(linkText, 1);

        assertTitleEquals(title, TEST_SPACE_TITLE);
    }

    public void testWidthParameterHtmlEncoded()
    {
        final long testWidthPageId = createPage(TEST_SPACE_KEY, "XSSTest", "{recently-updated:width=x\"</pre><script>alert(1)</script>}");
        viewPageById(testWidthPageId);
        assertEquals("width: x\"</pre><script>alert(1)</script>", getElementAttributeByXPath("//div[@class='wiki-content']//div[@class='recently-updated recently-updated-concise']", "style"));
    }

    public void testBlankExperienceHasClassAndWidth()
    {
        final long testBlankPageId = createPage(TEST_SPACE_KEY, "BlankTest", "{recently-updated:width=50%|types=blog}");
        viewPageById(testBlankPageId);
        assertEquals("width: 50%", getElementAttributeByXPath("//div[@class='wiki-content']//div[@class='recently-updated macro-blank-experience']", "style"));
    }

    private long setUserStatus(String s) throws JSONException {
        gotoPage(addFormSecurityToken("/status/update.action?text=" + s));
        getIndexHelper().update();
        return new JSONObject(getPageSource()).getLong("id");
    }

    private String getFormSecurityToken()
    {
        gotoPage("/"); // the security token lies within any Confluence page. We must ensure that we are on one else the extraction will fail
        return XsrfTokenInterceptor.REQUEST_PARAM_NAME + "=" + getElementAttributeByXPath("//meta[@id='atlassian-token']", "content");
    }

    private String addFormSecurityToken(String urlPath)
    {
        String separator = urlPath.contains("?") ? "&" : "?";
        return urlPath + separator + getFormSecurityToken();
    }
}
