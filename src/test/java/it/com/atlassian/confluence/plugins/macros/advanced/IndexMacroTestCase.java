package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.plugin.functest.helper.CommentHelper;
import org.apache.commons.lang.StringUtils;

public class IndexMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{

    protected long createComment(long pageId, String comment)
    {
        CommentHelper commentHelper = getCommentHelper();

        commentHelper.setParentId(pageId);
        commentHelper.setContent(comment);
        assertTrue(commentHelper.create());

        return commentHelper.getId();
    }

    public long createPageWithTitleInTestSpace(String title)
    {
        return createPage(TEST_SPACE_KEY, title, StringUtils.EMPTY);
    }

    private void assertGroupCount(String group, int count)
    {
        assertEquals(group + " ... " + count, getElementTextByXPath("//table//tr/td/a[@href='#index-" + group + "']/.."));
    }

    private void assertLinksInGroup(String group, String ... pageTitles)
    {
        for (int i = 0; i < pageTitles.length; ++i)
        {
            assertEquals(
                    pageTitles[i],
                    getElementTextByXPath("//td/h4/a[@name='index-" + group + "']/../../a[" + (i + 1) + "]")
            );
        }
    }

    public void testIndexGroupedByPageTitles()
    {
        for (char i = 'B', j = 'Z'; i <= j; ++i)
            createPageWithTitleInTestSpace(String.valueOf(i));

        for (int i = 0; i < 10; ++i)
            createPageWithTitleInTestSpace(String.valueOf(i));

        long testPageId = createPage(TEST_SPACE_KEY, "A", "{index}");


        getIndexHelper().update();
        viewPageById(testPageId);

        assertEquals("Space Index", getElementTextByXPath("//div[@class='wiki-content']/h2"));

        assertGroupCount("0-9", 10);
        assertLinksInGroup("0-9", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

        for (char i = 'A', j = 'Z'; i <= j; ++i)
        {
            String group = String.valueOf(i);

            assertGroupCount(group, StringUtils.equals("T", group) ? 2 : 1); /* 2 for T because one is automatically created for the test space home page, 'Test Space Home' */
            assertLinksInGroup(group, group);
        }
    }
}
