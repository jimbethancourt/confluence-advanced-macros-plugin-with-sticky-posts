package it.com.atlassian.confluence.plugins.macros.advanced;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.text.DecimalFormat;


public class NavmapMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private String label = "label";

    private void assertNavMapCell(int rowIndex, int colIndex, String pageTitle)
    {
        assertEquals(
                pageTitle,
                getElementTextByXPath("//div[@class='wiki-content']/table[@class='navMapContainer']//tr[" + (rowIndex + 1) + "]/td[" + (colIndex + 1) + "]/center/a")
        );
    }

    public void testNavmapShowsAllContentWithSpecifiedGlobalLabel()
    {
        DecimalFormat numberFormat = new DecimalFormat("00");


        /* Create lots of pages */
        for (int i = 0; i < 60; ++i)
            createPage(TEST_SPACE_KEY, numberFormat.format(i), StringUtils.EMPTY, Arrays.asList(label));

        long testPageId = createPage(TEST_SPACE_KEY, "testNavmapShowsAllContentWithSpecifiedGlobalLabel", "{navmap:" + label + "}");

        viewPageById(testPageId);

        int row = -1;
        for (int i = 0; i < 60; ++i)
        {
            int column = i % 5;
            row = i % 5 == 0 ? row + 1 : row;

            assertNavMapCell(row, column, numberFormat.format(i));
        }
    }

    public void testNavmapWithTitle()
    {
        /* Create some pages */
        for (int i = 0; i < 10; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, Arrays.asList(label));

        String navmapTitle = "navmapTitle";
        long testPageId = createPage(TEST_SPACE_KEY, "testNavmapWithTitle", "{navmap:" + label + "|title=" + navmapTitle + "}");

        viewPageById(testPageId);

        assertEquals(
                navmapTitle,
                getElementTextByXPath("//div[@class='wiki-content']/table[@class='navMapContainer']//th")
        );

        int row = 0;
        for (int i = 0; i < 9; ++i)
        {
            int column = i % 5;
            row = i % 5 == 0 ? row + 1 : row;

            assertNavMapCell(row, column, String.valueOf(i));
        }
    }

    public void testNavmapWithCustomTableWidth()
    {
        /* Create some pages */
        for (int i = 0; i < 10; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, Arrays.asList(label));

        int tableWidth = 2;
        long testPageId = createPage(TEST_SPACE_KEY, "testNavmapWithCustomTableWidth", "{navmap:" + label + "|wrapAfter=" + tableWidth + "}");

        viewPageById(testPageId);

        int row = -1;
        for (int i = 0; i < 9; ++i)
        {
            int column = i % tableWidth;
            row = i % tableWidth == 0 ? row + 1 : row;

            assertNavMapCell(row, column, String.valueOf(i));
        }
    }

    public void testNavmapWithCustomCellDimensions()
    {
        /* Create some pages */
        for (int i = 0; i < 10; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, Arrays.asList(label));

        int tableCellWidth = 100;
        int tableCellHeight = 100;
        long testPageId = createPage(TEST_SPACE_KEY, "testNavmapWithCustomCellDimensions", "{navmap:" + label + "|cellWidth=" + tableCellWidth + "|cellHeight=" + tableCellHeight + "}");

        viewPageById(testPageId);

        int row = -1;
        for (int i = 0; i < 9; ++i)
        {
            int column = i % 5;
            row = i % 5 == 0 ? row + 1 : row;

            assertNavMapCell(row, column, String.valueOf(i));
        }

        assertEquals(
                String.valueOf(tableCellWidth),
                getElementAttributeByXPath("//div[@class='wiki-content']/fieldset/input[@name='navmapCellWidth']", "value")
        );

        assertEquals(
                String.valueOf(tableCellHeight),
                getElementAttributeByXPath("//div[@class='wiki-content']/fieldset/input[@name='navmapCellHeight']", "value")
        );
    }

    public void testNavmapWillNotShowDeletedPage()
    {
        String pageToDelete = "pageToDelete";

        // Create a few pages
        for (int i = 0; i < 2; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, Arrays.asList(label));

        long testPageToDeleteId = createPage(TEST_SPACE_KEY, pageToDelete, StringUtils.EMPTY, Arrays.asList(label));

        long testPageId = createPage(TEST_SPACE_KEY, "testNavmapWithTitle", "{navmap:" + label + "}");

        viewPageById(testPageId);

        assertNavMapCell(0, 0, String.valueOf(0));
        assertNavMapCell(0, 1, String.valueOf(1));
        assertNavMapCell(0, 2, pageToDelete);

        deletePage(testPageToDeleteId);

        viewPageById(testPageId);

        assertNavMapCell(0, 0, String.valueOf(0));
        assertNavMapCell(0, 1, String.valueOf(1));

        assertLinkNotPresentWithExactText(pageToDelete);
    }
}
